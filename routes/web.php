<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth']], function()
{
    Route::post('/api/photo', 'PhotoController@storePhoto')->name('photo.store');

    /*
     * Ingredients
     */
    Route::group(['prefix' => 'ingredients'], function()
    {
        Route::get('/{page?}', 'IngredientsController@getIngredientsList')->name('ingredients.getList')->where(['page' => '[0-9]+']);
        Route::post('/{page?}', 'IngredientsController@postIngredientsList')->name('ingredients.postList')->where(['page' => '[0-9]+']);
        Route::get('/create', 'IngredientsController@getAddIngredient')->name('ingredients.getAdd');
        Route::post('/create', 'IngredientsController@postAddIngredient')->name('ingredients.postAdd');
        Route::get('/edit/{id}', 'IngredientsController@getEditIngredient')->name('ingredients.getEdit')->where(['id' => '[0-9]+']);
        Route::post('/edit/{id}', 'IngredientsController@postEditIngredient')->name('ingredients.postEdit')->where(['id' => '[0-9]+']);
        Route::get('/delete/{id}', 'IngredientsController@getDeleteIngredient')->name('ingredients.getDelete')->where(['id' => '[0-9]+']);
        Route::post('/delete/{id}', 'IngredientsController@postDeleteIngredient')->name('ingredients.postDelete')->where(['id' => '[0-9]+']);
    });

    Route::get('/api/ingredients', 'IngredientsController@getIngredientsListJson')->name('ingredients.getListJson');
    Route::post('/api/ingredient/create', 'IngredientsController@postCreateIngredient')->name('api.ingredients.postAdd');

    /*
     * Recipes
     */
    Route::group(['prefix' => 'recipes'], function() {
        Route::post('/getTemplate', 'RecipesController@getRecipeRow')->name('recipes.getRecipeTemplate');
        Route::post('/getExistingTemplate', 'RecipesController@getExistingRecipeRow')->name('recipes.getExistingRecipeTemplate');
        Route::get('/{id}', 'RecipesController@getRecipeDetails')->name('recipes.getDetails')->where(['id' => '[0-9]+']);
        Route::get('/create', 'RecipesController@getAddRecipe')->name('recipes.getAdd');
        Route::post('/create', 'RecipesController@postAddRecipe')->name('recipes.postAdd');
        Route::get('/edit/{id}', 'RecipesController@getEditRecipe')->name('recipes.getEdit')->where(['id' => '[0-9]+']);
        Route::post('/edit/{id}', 'RecipesController@postEditRecipe')->name('recipes.postEdit')->where(['id' => '[0-9]+']);
        Route::get('/delete/{id}', 'RecipesController@getDeleteRecipe')->name('recipes.getDelete')->where(['id' => '[0-9]+']);
        Route::post('/delete/{id}', 'RecipesController@postDeleteRecipe')->name('recipes.postDelete')->where(['id' => '[0-9]+']);

    });
    Route::get('/ulubione/{page?}', 'RecipesController@getFavourites')->name('recipes.getFavourites')->where(['page' => '[0-9]+']);
    Route::get('/lista-zakupow', 'RecipesController@getShoppingList')->name('recipes.getShoppingList');

    Route::get('/api/recipes', 'RecipesController@getRecipesListJson')->name('recipes.getListJson');
    Route::post('/api/recipe/favourite/{id}', 'RecipesController@postSetFavourite')->name('recipes.postSetFavourite');
    Route::post('/api/recipe/rate/{id}', 'RecipesController@postRate')->name('recipes.postRate');
    Route::post('/api/addToShoppingList', 'RecipesController@postAddIngredientToShoppingList')->name('recipes.postAddIngredientToShoppingList');
    Route::post('/api/removeFromShoppingList', 'RecipesController@postRemoveIngredientFromShoppingList')->name('recipes.postRemoveIngredientFromShoppingList');

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin']], function() {
        Route::get('/', 'DashboardController@getIndex')->name('admin.dashboard.getIndex');

        Route::group(['prefix' => 'users'], function () {
            Route::get('/{id?}', 'UsersController@getIndex')->name('admin.users.getIndex')->where(['id' => '[0-9]+']);

            Route::get('/create', 'UsersController@getCreate')->name('admin.users.getCreate');
            Route::post('/create', 'UsersController@postCreate')->name('admin.users.postCreate');

            Route::get('/edit/{id}', 'UsersController@getEdit')->name('admin.users.getEdit')->where(['id' => '[0-9]+']);
            Route::post('/edit/{id}', 'UsersController@postEdit')->name('admin.users.postEdit')->where(['id' => '[0-9]+']);

            Route::get('/delete/{id}', 'UsersController@getDelete')->name('admin.users.getDelete')->where(['id' => '[0-9]+']);
            Route::post('/delete/{id}', 'UsersController@postDelete')->name('admin.users.postDelete')->where(['id' => '[0-9]+']);

            Route::post('/activate/{id}', 'UsersController@postActivate')->name('admin.users.postActivate')->where(['id' => '[0-9]+']);
        });

        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', 'CategoriesController@getIndex')->name('admin.categories.getIndex');

            Route::get('/create/{id?}', 'CategoriesController@getCreate')->name('admin.categories.getCreate')->where(['id' => '[0-9]+']);
            Route::post('/create/{id?}', 'CategoriesController@postCreate')->name('admin.categories.postCreate')->where(['id' => '[0-9]+']);

            Route::get('/edit/{id}', 'CategoriesController@getEdit')->name('admin.categories.getEdit')->where(['id' => '[0-9]+']);
            Route::post('/edit/{id}', 'CategoriesController@postEdit')->name('admin.categories.postEdit')->where(['id' => '[0-9]+']);

            Route::get('/delete/{id}', 'CategoriesController@getDelete')->name('admin.categories.getDelete')->where(['id' => '[0-9]+']);
            Route::post('/delete/{id}', 'CategoriesController@postDelete')->name('admin.categories.postDelete')->where(['id' => '[0-9]+']);

            Route::post('/tree-menu', 'CategoriesController@postGetTreeMenu')->name('admin.categories.postGetTreeMenu');
            Route::post('/sort-menu', 'CategoriesController@postSortMenu')->name('admin.categories.postSortMenu');
            Route::post('/tree/{id}', 'CategoriesController@postGetTree')->name('admin.categories.postGetTree')->where(['id' => '[0-9]+']);
        });
    });

    Route::group(['prefix' => 'konto'], function() {
        Route::get('/', 'UsersController@getIndex')->name('users.getIndex');
        Route::get('/dane', 'UsersController@getEdit')->name('users.getEdit');
        Route::post('/dane', 'UsersController@postEdit')->name('users.postEdit');
        Route::get('/haslo', 'UsersController@getPassword')->name('users.getPassword');
        Route::post('/haslo', 'UsersController@postPassword')->name('users.postPassword');
        Route::get('/ustawienia', 'UsersController@getSettings')->name('users.getSettings');
        Route::post('/ustawienia', 'UsersController@postSettings')->name('users.postSettings');
        Route::get('/usun', 'UsersController@getDelete')->name('users.getDelete');
        Route::post('/usun', 'UsersController@postDelete')->name('users.postDelete');
    });

    Route::get('/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@getRecipesList')->name('home1');
    Route::get('/k/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@getRecipesList')->name('home');
    Route::get('/k/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@getRecipesList')->name('recipes.getList');
    Route::post('/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@postRecipesList');
    Route::post('/k/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@postRecipesList')->name('recipes.postList');
    Route::post('/k/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', 'RecipesController@postRecipesList')->name('postHome');
});


