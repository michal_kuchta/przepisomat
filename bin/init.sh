#!/bin/bash

if [[ -z "$SSH_HOSTNAME" ]] || [[ -z "$SSH_DIRECTORY" ]] || [[ -z "$SSH_PRIVATE_KEY" ]]; then
    echo "Unsupported branch"
    exit
fi
if [[ -z "$SSH_PORT" ]]; then
    SSH_PORT=22
fi

# ========== Init variables ==========
REPO=$(basename $CI_BUILD_REPO .git)
SRV_ROOT=$SSH_DIRECTORY
SRV_RELEASES=$SRV_ROOT/releases
SRV_PATH=$SRV_RELEASES/build-$CI_BUILD_ID
SRV_CURRENT=$SRV_ROOT/current
DBNAME=$REPO$DBNAME
echo "Variables has been initialized\n\n"

# ========== Create package containing project files ==========
mkdir -p .bin
tar czpf .bin/package.tar.gz *
echo "Package has been created\n\n"

# ========== SSH - Prepare agent ==========
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | base64 --decode
ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 --decode)
mkdir -p ~/.ssh
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
echo "SSH agent has been prepared\n\n"

# ========== SSH - Prepare shared folders ==========
ssh -oStrictHostKeyChecking=no -T $SSH_HOSTNAME -p $SSH_PORT <<EOM
mkdir -p $SRV_PATH
mkdir -p $SRV_ROOT
mkdir -p $SRV_RELEASES
mkdir -p $SRV_ROOT/shared/
exit
EOM
echo "Shared folder had been prepared\n\n"

# ========== SFTP - Send package to server ==========
sftp $SSH_HOSTNAME  -oPort=$SSH_PORT <<EOM
put .bin/package.tar.gz $SRV_PATH
exit
EOM
echo "Package had been sent\n\n"

# ========== SSH -  ==========
ssh -oStrictHostKeyChecking=no -T $SSH_HOSTNAME -p $SSH_PORT <<EOM
set -e

# Extract files
tar -C $SRV_PATH -zxf $SRV_PATH/package.tar.gz && echo "Files extracted from archive $SRV_PATH/package.tar.gz."
rm $SRV_PATH/package.tar.gz && echo "File $SRV_PATH/package.tar.gz removed."

# Link shared
ln -sf $SRV_ROOT/shared/storage $SRV_PATH/public && echo "Symbolic link to storage folder created."
ln -sf $SRV_ROOT/shared/.env $SRV_PATH && echo "Symbolic link to config created."

#composer
cd $SRV_PATH
/usr/local/bin/composer install --no-dev --no-scripts --no-interaction

#node
npm install --quiet
npm run prod --quiet

echo "Running migrations"
php artisan migrate

# Run supervisor
echo "Running supervisor"
supervisorctl update
supervisorctl stop websockets
supervisorctl start websockets

cd ~

# Switch live version
rm -f $SRV_CURRENT && echo "Symbolic link to current release folder $SRV_CURRENT removed."
ln -s $SRV_PATH $SRV_CURRENT && echo "Symbolic link to new release folder $SRV_PATH created."

# Cleanup old releases
cd $SRV_RELEASES
ls -1 --sort=time | tail -n +6 | xargs rm -rf - && echo "Older releases removed."
echo "New release has been linked\n\n"
exit
EOM

# ========== Remove package containing project files ==========
rm .bin/package.tar.gz
rm -r .bin
