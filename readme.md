## Uruchamianie supervisora
apt install supervisor

cd /etc/supervisor/conf.d<br>
touch websockets.conf

<h5>Zawartość pliku websockets.conf</h5>
[program:websockets]<br>
command=/usr/bin/php /root/mkuchta/przepisomat/artisan websockets:serve<br>
numprocs=1<br>
autostart=true<br>
autorestart=true<br>
user=laravel-echo<br>
redirect_stderr=true<br>
stdout_logfile=/root/mkuchta/przepisomat/storage/logs/supervisor.log

<h5>Po utworzeniu pliku wykonać polecenia</h5>
supervisorctl update
supervisorctl start websockets