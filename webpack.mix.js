const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var node_modules = 'node_modules';
var destination_css = 'public/vendor/css';
var destination_js = 'public/vendor/js';
var resources = 'resources/assets';
var webfonts = 'public/webfonts';

mix
    .js(resources + '/js/app.js', destination_js)
    .js(resources + '/js/tinymce/recipes.js', destination_js)
    .sass(resources + '/sass/app.scss', destination_css)
    .copy(resources + '/langs', destination_js + '/langs')
    .copy(resources + '/js/vendor', destination_js)
    .copy(resources + '/images', 'public/images')
    .copy(resources + '/css', destination_css)
    .copy(node_modules + '/grapesjs/dist/grapes.min.js', destination_js)
    .copy(node_modules + '/grapesjs/dist/fonts', webfonts)
    .copy(node_modules + '/grapesjs/dist/css/grapes.min.css', destination_css)
    .copy(node_modules + '/@fortawesome/fontawesome-free/css/all.css', destination_css)
    .copy(node_modules + '/@fortawesome/fontawesome-free/webfonts', webfonts);

mix.scripts([
        destination_js + '/**/*.js'
    ], 'public/js/app.js')
    .styles([
        destination_css + '/**/*.css'
    ], 'public/css/app.css');