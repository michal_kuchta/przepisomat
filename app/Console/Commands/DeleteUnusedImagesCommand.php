<?php

namespace App\Console\Commands;

use App\Jobs\DeleteUnusedImagesJob;
use Illuminate\Console\Command;

class DeleteUnusedImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photos:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes unused photos from database and disk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new DeleteUnusedImagesJob());
    }
}
