<?php
namespace App\Helpers;

/**
 * Klasa generujaca paginację dla tabeli
 * @package App\Helpers
 */
class Pager
{
    private $totalResults = 0; /** @var int ilość wszystkich wyników */
    private $limit = 10; /** @var int limit wyników na stronie */
    private $currentPage = 1; /** @var int identyfikator obecnej strony */
    private $totalPages = 0; /** @var int ilość wszystkich stron */
    private $offset = 0; /** @var int liczba określająca od którego elementu wyświetlić listę  */
    private $url = ''; /** @var string adres url do tworzenia adresów w paginacji */

    /**
     * Konstruktor ustawiający dane domyślne
     * @see setTotalResults()
     * @see setLimit()
     * @see setPage()
     * @param int $totalResults ilość wszystkich wyników
     * @param int $limit limit wyników na stronie
     * @param int $page identyfikator obecnej strony
     */
    public function __construct($totalResults = 0, $limit = 10, $page = 1)
    {
        $this->setTotalResults($totalResults);
        $this->setLimit($limit);
        $this->setPage($page);
    }

    /**
     * Ustawia ilość wszystkich wyników w tabeli oblicza offset
     * @see setOffset()
     * @param int $totalResults ilość wszystkich wyników
     * @return Pager $this
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
        $this->setOffset();

        return $this;
    }

    /**
     * Ustawia limit wyników wyświetlanych w tabeli
     * @param int $limit ilość wyników wyświetlanych na stronie
     * @return Pager $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        $this->setOffset();

        return $this;
    }

    /**
     * Ustawia numer obecnej strony jeżeli został podany, jeżeli nie ustawia 1
     * @param null|int $page numer obecnej strony
     * @return Pager $this
     */
    public function setPage($page = null)
    {
        $this->currentPage = $page ?? 1;
        $this->setOffset();

        return $this;
    }

    /**
     * Ustawia adres URL na podstawie którego będą generowane linki dla paginacji
     * @param string $url adres URL
     * @return Pager $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Ustawia numer wiersza od którego mają zostać wyświetlone wyniki
     */
    private function setOffset()
    {
        $page = $this->currentPage - 1 > 0 ? $this->currentPage - 1 : 0;
        $this->offset = $page * $this->limit;

        $this->totalPages = ceil($this->totalResults / $this->limit);
    }

    /**
     * Zwraca adres URL
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Zwraca offset
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Zwraca limit wierszy w tabeli
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Zwraca ilość stron
     * @return int
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * Zwraca numer obecnej strony
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * Zwraca ilość wszystkich wierszy
     * @return int
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * Zwraca kod HTML paginacji
     * @return string kod HTML
     */
    public function render()
    {
        return $this->__toString();
    }

    /**
     * Wywołana zostaje przy próbie wyświetlenia obiektu
     * @return string kod HTML
     */
    public function __toString()
    {
        return view('partials/pager', ['pager' => $this])->render();
    }
}
