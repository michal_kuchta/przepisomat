<?php

function __($original)
{
    $text = trans($original, [], null);

    if (func_num_args() === 1)
    {
        return $text;
    }

    $args = array_slice(func_get_args(), 1);

    return vsprintf($text, is_array($args[0]) ? $args[0] : $args);
}

function from($elements)
{
    return collect($elements);
}

/**
 * @param array $values
 * @param array $exclude
 * @param string $separator
 * @return string
 */
function hashJoin(array $values, array $exclude = ['0'], $separator = '#')
{
    $values = array_filter($values, function($var) use ($exclude)
    {
        return !in_array($var, $exclude);
    });

    return $separator.implode($separator, $values).$separator;
}

/**
 * @param string $value
 * @param string $separator
 * @return array
 */
function hashSplit($value, $separator = '#')
{
    return array_filter(explode($separator, trim($value, $separator)), function($var)
    {
        return !empty($var);
    });
}