<?php namespace App\Helpers\Eloquent;

use App\Helpers\Eloquent\Interfaces\IHierarchical;

class HierarchicalObserver
{
    /**
     * @param IHierarchical $model
     */
    private function correctParentId(IHierarchical $model)
    {
        if (empty($model->parent_id))
        {
            $model->parent_id = null;
        }
    }

    /**
     * @param IHierarchical $model
     * @throws \Exception
     */
    private function updateSelf(IHierarchical $model)
    {
        $model->level = 1;
        $model->ancestors = '';

        if (!empty($model->parent_id))
        {
            $parent = \DB::table($model->getTable())->find($model->parent_id, ['id', 'level', 'ancestors']);

            if (empty($parent))
            {
                throw new \Exception(__('Nie znaleziono rekordu rodzica w bazie.'));
            }

            $model->level = $parent->level + 1;
            $model->ancestors = rtrim($parent->ancestors, '#').'#'.$parent->id.'#';
        }
    }

    /**
     * @param IHierarchical $model
     */
    private function updateAncestors(IHierarchical $model)
    {
        if (!empty($model->parent_id))
        {
            $this->updateParent($model, $model->parent_id);
        }

        $parent_id = $model->getOriginal('parent_id');

        if (!empty($parent_id) && $parent_id != $model->parent_id)
        {
            $this->updateParent($model, $parent_id);
        }
    }

    /**
     * @param IHierarchical $model
     * @param $parent_id
     */
    private function updateParent(IHierarchical $model, $parent_id)
    {
        $query = \DB::table($model->getTable());

        $count = $query
            ->where('parent_id', $parent_id)
            ->count();

        \DB::table($model->getTable())
            ->where('id', $parent_id)
            ->update(['has_children' => ($count > 0)]);
    }

    /**
     * @param IHierarchical $model
     */
    private function updateDescendants(IHierarchical $model)
    {
        if ($model->has_children)
        {
            $level = $model->level - $model->getOriginal('level');
            $original = rtrim($model->getOriginal('ancestors'), '#').'#'.$model->id.'#';
            $ancestors = rtrim($model->ancestors, '#').'#'.$model->id.'#';

            \DB::table($model->getTable())
                ->where('parent_id', $model->id)
                ->update([
                    'level' => \DB::raw("level + ".$level),
                    'ancestors' => \DB::raw("REPLACE(ancestors, '".$original."', '".$ancestors."')")
                ]);
        }
    }

    /**
     * @param IHierarchical $model
     * @throws \Exception
     */
    public function creating(IHierarchical $model)
    {
        $this->correctParentId($model);
        $this->updateSelf($model);
    }

    /**
     * @param IHierarchical $model
     */
    public function created(IHierarchical $model)
    {
        $this->updateAncestors($model);
    }

    /**
     * @param IHierarchical $model
     * @throws \Exception
     */
    public function updating(IHierarchical $model)
    {
        $this->correctParentId($model);
        $this->updateSelf($model);
    }

    /**
     * @param IHierarchical $model
     */
    public function updated(IHierarchical $model)
    {
        $this->updateAncestors($model);
        $this->updateDescendants($model);
    }

    /**
     * @param IHierarchical $model
     */
    public function deleting(IHierarchical $model)
    {
        $this->correctParentId($model);
    }

    /**
     * @param IHierarchical $model
     */
    public function deleted(IHierarchical $model)
    {
        $this->updateAncestors($model);

        // Jeśli $model został fizycznie usunięty, to usunąć wszystkie podrzędne
        $current = \DB::table($model->getTable())->find($model->id, ['id']);

        if ($current == null)
        {
            \DB::table($model->getTable())
                ->where('ancestors', 'LIKE', "%#$model->id#%")
                ->delete();
        }
    }

    /**
     * @param IHierarchical $model
     * @throws \Exception
     */
    public function restoring(IHierarchical $model)
    {
        $this->correctParentId($model);

        $ids = hashSplit($model->ancestors);
        
        $ancestors = \DB::table($model->getTable())
            ->whereIn('id', $ids)
            ->orderBy('level')
            ->get();

        if (count($ancestors) != count($ids))
        {
            $model->parent_id = null;

            \DB::table($model->getTable())
                ->where('id', $model->id)
                ->update([
                    'parent_id' => $model->parent_id
                ]);
        }

        $this->updateSelf($model);
    }

    /**
     * @param IHierarchical $model
     */
    public function restored(IHierarchical $model)
    {
        $this->updateAncestors($model);
        $this->updateDescendants($model);
    }

}
