<?php namespace App\Helpers\Eloquent\Relations;

/**
 * Class HasRelations
 *
 * @package App\Helpers\Eloquent\Relations
 * 
 * @mixin \Illuminate\Database\Eloquent\Model
 */
trait HasRelations
{
    /**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $foreignKey
     * @param  string|\Closure  $keysCallback
     * @return HasManyWithCallback
     */
    public function hasManyWithCallback($related, $foreignKey, $keysCallback)
    {
        /** @var \Eloquent $instance */
        $instance = new $related;

        return new HasManyWithCallback($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $keysCallback);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $foreignKey
     * @param  string  $keysProperty
     * @return HasManyWithArray
     */
    public function hasManyWithArray($related, $foreignKey, $keysProperty)
    {
        /** @var \Eloquent $instance */
        $instance = new $related;

        return new HasManyWithArray($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $keysProperty);
    }
} 