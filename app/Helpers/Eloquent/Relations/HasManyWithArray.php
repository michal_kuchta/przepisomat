<?php namespace App\Helpers\Eloquent\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class HasManyWithArray extends HasManyWithCallback
{
    /**
     * Create a new has one or many relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string $foreignKey
     * @param  string $keysProperty
     */
    public function __construct(Builder $query, Model $parent, $foreignKey, $keysProperty)
    {
        parent::__construct($query, $parent, $foreignKey, function($model) use ($keysProperty)
        {
            return $model->{$keysProperty};
        });
    }
}