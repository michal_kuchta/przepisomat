<?php namespace App\Helpers\Eloquent\Interfaces;

/**
 * Interface IHierarchical
 *
 * @property int $id
 * @property int $parent_id
 * @property string $ancestors
 * @property int $level
 * @method getTable()
 * @method getOriginal($key = null, $default = null)
 * @package Edito\Extensions\Eloquent\Interfaces
 */
interface IHierarchical
{
    /**
     * @return array
     */
    public function getAncestors();
    
    /**
     * @return array
     */
    public function getAffinity();
}