<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * App\RecipeIngredient
 *
 * @property int $recipe_id
 * @property int $ingredient_id
 * @property float $value
 * @property string $unit
 * @property int $id
 * @property-read \App\Ingredient $ingredient
 * @property-read \App\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeIngredient whereValue($value)
 * @mixin \Eloquent
 */
class RecipeIngredient extends Model
{
    protected $table = 'recipe_ingredients';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'recipe_id',
        'ingredient_id',
        'value',
        'unit'
    ];

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function recipe()
    {
        return $this->hasOne(Recipe::class, 'id', 'recipe_id');
    }

    public function addValue($value)
    {
        $this->value += $value;
    }

    public static function saveIngredientsInRecipe($recipe, $recipeId)
    {
        RecipeIngredient::query()->where('recipe_id', '=', $recipeId)->delete();
        if(array_has($recipe, 'ingredient') && count(array_get($recipe,'ingredient', [])) > 0)
        {
            foreach(array_get($recipe, 'ingredient') as $ingredientId => $ingredientData)
            {
                $entity = new RecipeIngredient();
                $entity->ingredient_id = $ingredientId;
                $entity->recipe_id = $recipeId;
                $entity->value = $ingredientData['value'];
                $entity->unit = $ingredientData['unit'];
                $entity->save();
            }
        }
    }

    public function getValue($base = false)
    {
        if ($base)
        {
            $value = $this->convertValueToBase();
            return $value;
        }

        return $this->value;
    }

    public function getUnitById($id)
    {
        return $this->ingredient->units($id);
    }

    public function getBaseUnit($unit = null)
    {
        if (is_null($unit))
            $unit = $this->getUnitById($this->unit);

        if (Arr::has($unit, 'child'))
        {
            return $this->getBaseUnit($this->getUnitById($unit['child']));
        }

        return $unit;
    }

    public function getBaseAbbreviation()
    {
        return $this->ingredient->getBaseAbbreviation($this->getBaseUnit()['id'], $this->getValue(true));
    }

    public function convertValueToBase($unit = null, $value = null)
    {
        if (is_null($value))
            $value = $this->value;

        if (is_null($unit))
            $unit = $this->getBaseUnit();

        if (!Arr::has($this->getUnitById($this->unit), 'child'))
            return $value;

        if (Arr::has($unit, 'parent') && $unit['parentId'] != $this->unit)
            $value = $this->convertValueToBase($this->getUnitById($unit['parentId']), $value);

        if (Arr::has($unit, 'multiplicity'))
            $value *= Arr::get($unit, 'multiplicity', 1);

        return $value;
    }

}
