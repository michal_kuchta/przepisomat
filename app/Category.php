<?php

namespace App;

use App\Helpers\Eloquent\HierarchicalObserver;
use App\Helpers\Eloquent\Interfaces\IHierarchical;
use App\Helpers\Eloquent\Relations\HasRelations;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements IHierarchical
{
    use HasRelations, CategoryHierarchical;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'parent_id',
        'ancestors',
        'level',
        'position',
        'symbol',
        'is_active'
    ];

    public static function boot()
    {
        parent::boot();

        self::observe(new HierarchicalObserver());
    }

    public static function getList(array $expanded = [])
    {
        return self::query()
            ->where(function($sql) use ($expanded)
            {
                /** @var self $sql */
                $sql->whereNull('parent_id')
                    ->orWhereIn('parent_id', $expanded);
            })
            ->orderBy('position', 'asc')
            ->orderBy('id', 'asc')
            ->get();
    }

    private static function options(array $categories, $root, $level)
    {
        $options = [];
        $nbsp = chr(194).chr(160);

        $items = from($categories)
            ->filter(function($p) use($root) { return $p['parent_id'] == $root; })
            ->toArray();

        foreach ($items as $item)
        {
            $pad = (4 * strlen($nbsp) * $level) + strlen($item['name']);
            $options[$item['id']] = str_pad($item['name'], $pad, $nbsp, STR_PAD_LEFT);
            $options = $options + self::options($categories, $item['id'], $level + 1);
        }

        return $options;
    }

    public static function getOptions($exceptId = 0)
    {
        $query = self::query();

        $categories = $query
            ->where('id', '!=', $exceptId)
            ->select(['id', 'parent_id', 'name'])
            ->orderBy('position', 'asc')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        return self::options($categories, null, 0);
    }

    public static function getByIds(array $ids)
    {
        return self::query()->whereIn('id', $ids)->get();
    }

    public function getUrl()
    {
        $symbols = [];

        foreach ($this->ancestors_list as $ancestor)
        {
            $symbols[] = $ancestor->symbol;
        }

        $symbols[] = $this->symbol . ',' . $this->id;
        return route('recipes.getList', $symbols);
    }

    public function getIntendedName()
    {
        $level = $this->level - 1;

        return str_repeat(" ", $level * 4) . $this->name;
    }

    public static function getCategoriesTree($parent_id = null, $level = 1)
    {
        $categories = self::query()->where('level', $level);

        if (!is_null($parent_id) && $parent_id > 0)
        {
            $categories->where('ancestors', 'like', '%#'.$parent_id.'#%');
        }

        $categories = $categories->get();

        $categories->each(function(Category &$item) use ($level){
            if ($item->has_children)
            {
                $item->setRelation('childrens', self::getCategoriesTree($item->id, $level+1));
            }
        });

        return $categories;
    }
}

trait CategoryHierarchical
{
    /**
     * @return array
     */
    public function getAncestors()
    {
        return collect(hashSplit($this->ancestors))
            ->map(function($id) { return (int)$id; })
            ->toArray();
    }

    /**
     * @return array
     */
    public function getAffinity()
    {
        return array_merge($this->getAncestors(), [intval($this->id)]);
    }

    // --------------------------------------------------

    /**
     * @return \Edito\Extensions\Eloquent\Relations\HasManyWithCallback
     */
    public function ancestors_list()
    {
        return $this->hasManyWithCallback(Category::class, 'id', 'getAncestors');
    }

    /**
     * @return bool
     */
    public function isTrashed()
    {
        $ids = $this->getAncestors();
        $ancestors = self::whereIn('id', $ids)->count();
        if (count($ids) != $ancestors) return true;

        return false;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return (
            $this->is_active == true &&
            $this->ancestors_list->where('is_active', '=', true)->count() == count($this->getAncestors())
        );
    }

    /**
     * @return string
     */
    public function getRelativePosition()
    {
        return str_pad($this->position, 5, '0', STR_PAD_LEFT);
    }

    /**
     * @return string
     */
    public function getAbsolutePosition()
    {
        $position = from($this->ancestors_list)
            ->orderBy(function(Category $v)
            {
                return $v->level;
            })
            ->select(function(Category $v)
            {
                return $v->getRelativePosition();
            })
            ->toArray();

        $position[] = $this->getRelativePosition();

        return implode('.', $position);
    }
}