<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * App\Ingredient
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\RecipeIngredient[] $recipes
 * @property-read int|null $recipes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Ingredient extends Model
{
    protected $table = 'ingredients';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = [
        'name'
    ];

    public function recipes()
    {
        return $this->hasMany(RecipeIngredient::class, 'ingredient_id', 'id');
    }

    public static function units($id = null)
    {
        $units = [
            '1' => [
                'id' => '1',
                'name' => 'Kilogramy',
                'abbreviation' => 'kg',
                'child' => '2'
            ],
            '2' => [
                'id' => '2',
                'name' => 'Dekagramy',
                'abbreviation' => 'dag',
                'parentId' => '1',
                'multiplicity' => '100',
                'child' => '3'
            ],
            '3' => [
                'id' => '3',
                'name' => 'Gramy',
                'abbreviation' => 'g',
                'parentId' => '2',
                'multiplicity' => '10'
            ],
            '4' => [
                'id' => '4',
                'name' => 'Litry',
                'abbreviation' => 'l',
                'child' => '5'
            ],
            '5' => [
                'id' => '5',
                'name' => 'Mililitry',
                'abbreviation' => 'ml',
                'parentId' => '4',
                'multiplicity' => '1000'
            ],
            '6' => [
                'id' => '6',
                'name' => 'Sztuki',
                'abbreviation' => 'szt',
            ],
            '7' => [
                'id' => '7',
                'name' => 'Pęczek',
                'abbreviation' => [
                    0 => 'pęczka',
                    1 => 'pęczek',
                    2 => 'pęczki',
                    3 => 'pęczków'
                ],
            ],
            '8' => [
                'id' => '8',
                'name' => 'Szczypta',
                'abbreviation' => [
                    0 => 'szczypty',
                    1 => 'szczypta',
                    2 => 'szczypty',
                    3 => 'szczypt'
                ],
            ],
            '9' => [
                'id' => '9',
                'name' => 'Łyżeczka',
                'abbreviation' => [
                    0 => 'łyżeczki',
                    1 => 'łyżeczka',
                    2 => 'łyżeczki',
                    3 => 'łyżeczek'
                ],
            ],
            '10' => [
                'id' => '10',
                'name' => 'Łyżka',
                'abbreviation' => [
                    0 => 'łyżki',
                    1 => 'łyżka',
                    2 => 'łyżki',
                    3 => 'łyżek'
                ],
            ],
            '11' => [
                'id' => '11',
                'name' => 'Szklanka',
                'abbreviation' => [
                    0 => 'szklanki',
                    1 => 'szklanka',
                    2 => 'szklanki',
                    3 => 'szklanek'
                ],
            ],
            '12' => [
                'id' => '12',
                'name' => 'Ząbek',
                'abbreviation' => [
                    0 => 'ząbka',
                    1 => 'ząbek',
                    2 => 'ząbki',
                    3 => 'ząbków'
                ],
            ],
            '13' => [
                'id' => '13',
                'name' => 'Opakowanie',
                'abbreviation' => 'op',
            ]
        ];

        if($id && Arr::has($units, $id))
        {
            $unit = Arr::get($units, $id, []);
            if(Arr::has($unit, 'parentId'))
            {
                $unit['parent'] = self::units($unit['parentId']);
            }
            return $unit;
        }

        return $units;
    }

    public static function getAbbreviation($id, $value = null)
    {
        $abbreviation = self::units($id)['abbreviation'];

        if (is_array($abbreviation))
        {
            if (!is_null($value) && !is_int($value)) return $abbreviation[0];
            else if (!is_null($value) && is_int($value) && $value == 1) return $abbreviation[1];
            else if (!is_null($value) && is_int($value) && $value > 1 && $value < 5) return $abbreviation[2];
            else if (!is_null($value) && is_int($value) && $value >= 5) return $abbreviation[3];
            else return $abbreviation[1];
        }

        return $abbreviation;
    }

    public function getBaseAbbreviation($id, $value)
    {
        return self::getAbbreviation($id, $value);
    }
}
