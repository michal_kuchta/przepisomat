<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    protected $table = 'shopping_list';
    protected $fillable = [
        'recipe_id', 'ingredient_id', 'user_id'
    ];

    public $timestamps = false;

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public static function isOnShoppingList($ingredient_id)
    {
        return ShoppingList::query()->where('user_id', auth()->user()->id)->where('ingredient_id', $ingredient_id)->count() > 0;
    }
}
