<?php

namespace App\Http\Controllers;

use App\Helpers\Pager;
use App\Photo;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class PhotoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function storePhoto(Request $request)
    {
        if($request->hasFile('file'))
        {
            $image = $request->file('file');
            $imageName = $image->hashName();

            $dir = storage_path('/app/public/recipes');
            if (!file_exists($dir))
            {
                mkdir($dir,0777,true);
            }

            $img = Image::make($image->path());
            $img->resize(500, null, function($constraint){
                $constraint->aspectRatio();
            })->save($dir . '/' . $imageName);

            $path = '/storage/recipes/' . $imageName;

            $photo = new Photo();
            $photo->url = $path;
            $photo->save();

            return response()->json(['id' => $photo->id, 'path' => $path]);
        }

        return response()->json(['path' => '', 'id' => '']);
    }

}
