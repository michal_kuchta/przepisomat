<?php

namespace App\Http\Controllers;

use App\Helpers\Pager;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getRecipesList($page = 1)
    {
        $filters = session()->get('filters@'.self::class, []);
        $count = Recipe::query()->count();

        $pager = new Pager($count, 10, $page);
        $pager->setUrl(route('ingredients.getList'));

        $query = Recipe::query();

        if(Arr::has($filters, 'query') && !empty(Arr::get($filters, 'query', '')))
        {
            $query->where('nazwa', 'LIKE', '%'.Arr::get($filters, 'query').'%');
        }

        $rows = $query->limit($pager->getLimit())->offset($pager->getOffset())->orderBy('nazwa')->get();

        return view('recipes/list', ['rows' => $rows, 'pager' => $pager, 'filters' => $filters]);
    }

    public function postRecipesList(Request $request, $page = 1)
    {
        $filters = $request->only(['query']);

        array_filter($filters, function($val, $key){
            return !empty($val);
        }, ARRAY_FILTER_USE_BOTH);

        session()->put('filters@'.self::class, $filters);

        return redirect(route('recipes.getList'));
    }

    public function store(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $file = $image->move(public_path('photos'), $imageName);

        $photo = new Photo();
        $photo->url = $file->getPath();
        $photo->save();

        return response()->json(['success' => ['name' => $imageName, 'id' => $photo->id]]);
    }

}
