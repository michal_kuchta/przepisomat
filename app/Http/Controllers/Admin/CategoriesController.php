<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class CategoriesController extends Controller
{
    public function __construct()
    {
        // Konfiguracja szablonu widoku
        view()->composer('admin.categories.layout', function(View $view)
        {
            $this->layoutComposer($view);
        });

        // Formularz edycji kategorii
        view()->composer('admin.categories.form', function(View $view)
        {
            $this->formComposer($view);
        });

        // Wszystko co potrzebne do drzewa kategorii
        view()->composer('admin.categories.tree*', function(View $view)
        {
            $this->treeComposer($view);
        });
    }

    private function layoutComposer(View $view)
    {
        $expanded = session()->get('categories.expanded', []);

        $view->with('categories', Category::getList($expanded));
    }

    private function formComposer(View $view)
    {
        $categories = Category::getOptions(array_get($view->getData(), 'model.id', 0));

        $view->with(compact('categories'));
    }

    private function treeComposer(View $view)
    {
        $advances = request()->input('advanced', true);
        $view->with('advanced', $advances);
        $view->with('expanded', session()->get('categories.expanded', []));

        if (!array_key_exists('currentCategoryId', $view->getData()))
        {
            $view->with('currentCategoryId', 0);
        }
    }

    public function getIndex()
    {
        return view('admin.categories.index');
    }

    private function categoryRules($create = true, $id = null)
    {
        $rules = [
            'name' => 'required',
        ];

        return $rules;
    }

    private function getCategories()
    {
        $categories = [
            0 => __('Brak')
        ];

        $categories = $categories + Category::query()->pluck('name', 'id')->toArray();

        return $categories;
    }

    public function getCreate(Request $request, $parentId = 0)
    {
        view()->share('currentCategoryId', $parentId);

        return view('admin.categories.form', [
            'rules' => $this->categoryRules(true),
            'model' => array_merge($request->old(), ['parent_id' => $parentId]),
            'ancestors' => collect()
        ]);
    }

    public function postCreate()
    {
        $this->validate(request(), $this->categoryRules(true));

        $entity = new Category();
        $entity->fill(request()->all());
        $entity->symbol = str_slug($entity->name);
        $entity->is_active = request()->get('is_active') == 1;
        $entity->save();

        flash()->success(__('Pomyślnie utworzono rekord w bazie danych'));

        return redirect()->to(route('admin.categories.getEdit', [$entity->id]));
    }

    public function getEdit($id)
    {
        $entity = Category::find($id);

        if (!$entity)
        {
            flash()->warning(__('Brak kategorii o podanym id'));
            return redirect()->to(route('admin.categories.getIndex'));
        }

        $entity->fill(request()->old());

        view()->share('currentCategoryId', $id);

        return view('admin.categories.form', [
            'model' => $entity,
            'rules' => $this->categoryRules(false, $id),
            'ancestors' => $entity->ancestors_list
        ]);
    }

    public function postEdit($id)
    {
        $entity = Category::find($id);

        if (!$entity)
        {
            flash()->warning(__('Brak kategorii o podanym id'));
            return redirect()->to(route('admin.categories.getIndex'));
        }

        $this->validate(request(), $this->categoryRules(false, $id));
        $entity->fill(request()->all());
        $entity->is_active = request()->get('is_active') == 1;

        $entity->save();

        flash()->success(__('Pomyślnie zaktualizowano rekord w bazie danych'));

        return redirect()->to(route('admin.categories.getEdit', [$entity->id]));
    }

    public function getDelete($id)
    {
        $entity = Category::find($id);

        if (!$entity)
        {
            flash()->warning(__('Brak kategorii o podanym id'));
            return redirect()->to(route('admin.categories.getIndex'));
        }

        return view('admin.categories.delete', ['model' => $entity]);
    }

    public function postDelete($id)
    {
        $entity = Category::find($id);

        if ($entity)
        {
            $entity->delete();
            flash()->success(__('Kategoria została usunięta'));
        }
        else
        {
            flash()->warning(__('Brak kategorii o podanym id'));
        }

        return redirect()->to(route('admin.categories.getIndex'));
    }

    public function postGetTree(Request $request, $id)
    {
        $expanded = session()->get('categories.expanded', []);

        if ($request->get('mode') == 'expanded')
        {
            if (($key = array_search($id, $expanded)) !== false)
            {
                unset($expanded[$key]);
            }
        }
        else
        {
            if (!in_array($id, $expanded))
            {
                array_push($expanded, $id);
            }
        }

        session()->put('categories.expanded', $expanded);

        if ($request->get('mode') == 'load')
        {
            $expanded = session()->get('categories.expanded', []);
            $model = Category::getList($expanded);

            return view('admin.categories.tree-nodes', [
                'model' => $model,
                'parent_id' => $id
            ]);
        }

        return response()->json(['status' => 'ok']);
    }

    public function postGetTreeMenu(Request $request)
    {
        $model = Category::query()->findOrFail($request->get('id'), ['id']);

        return view('admin.categories.tree-menu', compact('model'));
    }

    public function postSortMenu(Request $request)
    {
        $positions = $request->get('positions', []);
        $pages = Category::getByIds($positions);
        $positions = array_flip($positions);

        /** @var Category $page */
        foreach ($pages as $page)
        {
            $page->position = $positions[$page->id] + 1;
            $page->save();
        }

        return response()->json(['status' => 'ok']);
    }


}
