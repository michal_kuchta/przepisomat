<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param null $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex($page = null)
    {
        $query = User::query();

        $pager = new Pager($query->count(), 10, $page);
        $pager->setUrl(route('admin.users.getIndex'));

        $rows = $query->limit($pager->getLimit())->offset($pager->getOffset())->orderBy('name')->get();

        return view('admin/users/index', ['pager' => $pager, 'rows' => $rows]);
    }

    public function postActivate($id)
    {
        $user = User::findOrFail($id);

        if ($user->is_active)
            $user->is_active = false;
        else
            $user->is_active = true;

        $user->save();

        return redirect()->to(route('admin.users.getIndex'));
    }

    private function userRules($create = false)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];

        if($create)
            $rules['password'] = 'required';

        return $rules;
    }

    public function getCreate()
    {
        $user = new User();

        return view('admin.users.form', ['model' => $user, 'rules' => $this->userRules(true)]);
    }

    public function postCreate()
    {
        $this->validate(request(), $this->userRules(true));

        $user = new User();
        $user->fill(request()->only(['name', 'email', 'is_active']));
        $user->is_active = request()->get('is_active') == 1;
        $user->password = Hash::make(request()->get('password'));
        $user->save();

        flash()->success(__('Pomyślnie utworzono rekord w bazie danych'));

        return redirect()->to(route('admin.users.getEdit', [$user->id]));
    }

    public function getEdit($id)
    {
        $user = User::find($id);

        if (!$user)
        {
            flash()->warning(__('Brak użytkownika o podanym id'));
            return redirect()->to(route('admin.users.getIndex'));
        }

        return view('admin.users.form', ['model' => $user, 'rules' => $this->userRules()]);
    }

    public function postEdit($id)
    {
        $user = User::find($id);

        if (!$user)
        {
            flash()->warning(__('Brak użytkownika o podanym id'));
            return redirect()->to(route('admin.users.getIndex'));
        }

        $this->validate(request(), $this->userRules());
        $user->fill(request()->only(['name', 'email', 'is_active']));
        $user->is_active = request()->get('is_active') == 1;

        $user->save();

        flash()->success(__('Pomyślnie zaktualizowano rekord w bazie danych'));

        return redirect()->to(route('admin.users.getEdit', [$user->id]));
    }

    public function getDelete($id)
    {
        $user = User::find($id);

        if (!$user)
        {
            flash()->warning(__('Brak użytkownika o podanym id'));
            return redirect()->to(route('admin.users.getIndex'));
        }

        return view('admin.users.delete', ['model' => $user]);
    }

    public function postDelete($id)
    {
        $user = User::find($id);

        if ($user)
        {
            $user->delete();
            flash()->success(__('Użytkownik został usunięty'));
        }
        else
        {
            flash()->warning(__('Brak użytkownika o podanym id'));
        }
        return redirect()->to(route('admin.users.getIndex'));
    }
}
