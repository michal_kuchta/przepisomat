<?php

namespace App\Http\Controllers;

use App\Category;
use App\Events\NotificationEvent;
use App\Helpers\Pager;
use App\Ingredient;
use App\Photo;
use App\Recipe;
use App\RecipeIngredient;
use App\RecipePhoto;
use App\RecipeRecipe;
use App\RecipeStep;
use App\SharedRecipe;
use App\ShoppingList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RecipesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getRecipeDetails($id)
    {
        /**
         * @var Recipe $entity
         */
        $entity = Recipe::with(['ingredients', 'steps', 'steps.photo', 'photos', 'photos.photo', 'subRecipes'])
            ->where(function ($sql){
                $sql->orWhereIn('user_id', auth()->user()->getSharedUserIds())
                    ->orWhereIn('id', auth()->user()->getSharedRecipesIds());
            })
            ->findOrFail($id);

        return view('recipes/details', ['entity' => $entity]);
    }

    public function getRecipesList()
    {
        $args = func_get_args();
        $requestArgs = $args;
        $page = 1;
        $category = null;
        $slug = null;

        //Może to być już kategoria
        $lastArg = array_pop($args);

        //Jeżeli sama liczba, to znaczy że paginacja bez kategorii
        if (preg_match('/^[1-9]*$/', $lastArg))
        {
            $page = intval($lastArg);
            $lastArg = array_pop($args);
            array_pop($requestArgs);
        }

        //Jeżeli nie tylko liczby oraz nie jest nullem to jest to kategoria
        if (!is_null($lastArg) && preg_match('/,/', $lastArg))
        {
            list($slug, $category) = explode(',', $lastArg);
            $category = intval($category);
        }

        $filters = session()->get('filters@'.self::class, []);

        $query = Recipe::query()->with('user');

        if (!empty($category) && $category > 0)
        {
            $categories = Category::query()->orWhere('id', $category)->orWhere('ancestors', 'LIKE', "%#{$category}#%")->pluck('id')->toArray();

            $query->whereIn('category_id', $categories);
        }

        $query->where(function ($sql){
            $sql->orWhereIn('user_id', auth()->user()->getSharedUserIds())
                ->orWhereIn('id', auth()->user()->getSharedRecipesIds());
        });

        if(Arr::has($filters, 'query') && !empty(Arr::get($filters, 'query', '')))
        {
            $query->where('name', 'LIKE', '%'.Arr::get($filters, 'query').'%');
        }

        $count = $query->count();

        $pager = new Pager($count, 10, $page <= 0 ? 1 : $page);

        $pager->setUrl(route('recipes.getList', array_filter($requestArgs)));

        $query->limit($pager->getLimit())
            ->offset($pager->getOffset());

        $this->setOrderBy($query);

        $rows = $query->get();

        $categories = Category::getCategoriesTree();

        return view('recipes/list', ['rows' => $rows, 'pager' => $pager, 'filters' => $filters, 'categories' => $categories, 'sort' => session()->get('sort@'.self::class, ['date_desc'])]);
    }

    private function setOrderBy(Builder &$query)
    {
        $sort = session()->get('sort@'.self::class, ['date_desc']);

        if ($sort == 'date_desc')
        {
            $query->orderBy('created_at', 'desc');
        }
        elseif ($sort == 'date_asc')
        {
            $query->orderBy('created_at', 'asc');
        }
        elseif ($sort == 'name_asc')
        {
            $query->orderBy('name', 'asc');
        }
        elseif ($sort == 'name_desc')
        {
            $query->orderBy('name', 'desc');
        }
        else
        {
            $query->orderBy('id', 'desc');
        }
    }

    public function postRecipesList(Request $request, $page = 1)
    {
        $filters = $request->only(['query']);

        array_filter($filters, function($val, $key){
            return !empty($val);
        }, ARRAY_FILTER_USE_BOTH);

        session()->put('filters@'.self::class, $filters);

        session()->put('sort@'.self::class, $request->input('sort'));

        return redirect()->back();
    }

    private function getOldRecipesData()
    {
        $entities = collect();

        if (empty(request()->old()))
        {
            return $entities;
        }

        foreach (request()->old('recipe') as $recipe)
        {
            if (isset($recipe['id']) && !is_null($recipe['id']) && intval($recipe['id']) > 0)
                $oldRecipe = Recipe::query()->where('user_id', auth()->id())->where('id', $recipe['id'])->first();
            else
                $oldRecipe = new Recipe();

            $oldRecipe->name = $recipe['name'];
            $oldRecipe->description = $recipe['description'];
            $oldRecipe->portions = $recipe['portions'];
            $oldRecipe->preparing_time = $recipe['preparing_time'];
            $oldRecipe->difficulty = $recipe['difficulty'];

            if (isset($recipe['ingredient']) && !empty($recipe['ingredient']))
            {
                $oldIngredients = collect();

                foreach ($recipe['ingredient'] as $ingredient_id => $ingredient)
                {
                    if (!is_null($oldRecipe->id))
                        $oldIngredient = RecipeIngredient::query()->where('recipe_id', $oldRecipe->id)->where('ingredient_id', $ingredient_id)->first();
                    else
                        $oldIngredient = new RecipeIngredient();

                    $oldIngredient->recipe_id = $oldRecipe->id;
                    $oldIngredient->ingredient_id = $ingredient_id;
                    $oldIngredient->value = $ingredient['value'];
                    $oldIngredient->unit = $ingredient['unit'];

                    $oldIngredient->setRelation('ingredient', Ingredient::findOrFail($ingredient_id));

                    $oldIngredients->add($oldIngredient);
                }

                $oldRecipe->setRelation('ingredients', $oldIngredients);
            }

            if (isset($recipe['step']) && !empty($recipe['step']))
            {
                $oldSteps = collect();

                foreach ($recipe['step'] as $step_id => $step)
                {
                    if (!is_null($oldRecipe->id))
                        $oldStep = RecipeStep::query()->where('recipe_id', $oldRecipe->id)->where('position', $step_id)->first();
                    else
                        $oldStep = new RecipeStep();

                    $oldStep->recipe_id = $oldRecipe->id;
                    $oldStep->position = $step_id;
                    $oldStep->description = array_get($step, 'description', '');
                    $oldStep->photo_id = array_get($step, 'photo', '');

                    if (!empty($oldStep->photo_id))
                    {
                        $oldStep->setRelation('photo', Photo::findOrFail($oldStep->photo_id));
                    }

                    $oldSteps->add($oldStep);
                }

                $oldRecipe->setRelation('steps', $oldSteps);
            }

            if (isset($recipe['images']) && !empty($recipe['images']))
            {
                $oldImages = collect();

                foreach ($recipe['images'] as $image_id)
                {
                    if (!is_null($oldRecipe->id))
                        $oldPhoto = RecipePhoto::query()->where('recipe_id', $oldRecipe->id)->where('photo_id', $image_id)->first();
                    else
                        $oldPhoto = new RecipePhoto();

                    $oldPhoto->recipe_id = $oldRecipe->id;
                    $oldPhoto->photo_id = $image_id;

                    if (!empty($oldPhoto->photo_id))
                    {
                        $oldPhoto->setRelation('photo', Photo::findOrFail($oldPhoto->photo_id));
                    }

                    $oldImages->add($oldPhoto);
                }

                $oldRecipe->setRelation('photos', $oldImages);
            }

            $entities->add($oldRecipe);
        }

        return $entities;
    }

    public function getAddRecipe()
    {
        $entities = $this->getOldRecipesData();

        if ($entities->count() == 0)
        {
            $entities->add(new Recipe());
        }

        return view('recipes/form', ['entities' => $entities]);
    }

    public function getRecipeRow()
    {
        $id = request()->get('id');

        $entities = [];

        if (request()->has('recipe_id'))
        {
            $recipe = Recipe::query()
                ->where('id', '=', intval(request()->get('recipe_id')))
                ->where('user_id', '=', auth()->id())
                ->first();

            if ($recipe)
            {
                $entities[$id] = $recipe;
            }
            else
            {
                $entities[$id] = new Recipe();
            }
        }
        else
        {
            $entities[$id] = new Recipe();
        }

        return view('recipes.form', ['entities' => $entities])->render();
    }

    public function getExistingRecipeRow()
    {
        $id = request()->get('id');

        $view = view('recipes.existingRowForm', ['id' => $id, 'hidePhotos' => true])->render();
        return $view;
    }

    public function postAddRecipe(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $parent = null;
            foreach (request()->get('recipe', []) as $recipe)
            {
                if (isset($recipe['id']))
                    $entity = Recipe::query()->where('id', '=', $recipe['id'])->where('user_id', '=', auth()->id())->first();

                if (!isset($entity) || !$entity)
                    $entity = new Recipe();

                $entity->fill(array_only($recipe, ['name', 'description', 'portions', 'preparing_time', 'difficulty', 'category_id', 'estimated_price']));
                $entity->user_id = \Auth::id();
                $entity->save();

                if (is_null($parent))
                {
                    $parent = $entity->id;
                }
                else
                {
                    $recipeRecipe = new RecipeRecipe();
                    $recipeRecipe->parent_id = $parent;
                    $recipeRecipe->recipe_id = $entity->id;
                    $recipeRecipe->save();
                }

                RecipeIngredient::saveIngredientsInRecipe($recipe, $entity->id);
                RecipeStep::saveStepsInRecipe($recipe, $entity->id);
                RecipePhoto::savePhotosInRecipe($recipe, $entity->id);
                SharedRecipe::saveRecipe($recipe, $entity->id);
            }

            DB::commit();
        }
        catch (\Exception $e)
        {
            Log::error($e->getMessage());
            DB::rollBack();

            flash()->error(__('Wystąpił błąd podczas zapisywania przepisu. Spróbuj ponownie później'));
            return redirect()->refresh()->withInput();
        }

        return redirect()->to(route('recipes.getList'));
    }

    public function getEditRecipe($id)
    {
        $entities = $this->getOldRecipesData();

        if ($entities->count() == 0)
        {
            $entity = Recipe::with(['ingredients', 'steps', 'steps.photo', 'photos', 'photos.photo'])
                ->where('id', $id)
                ->where('user_id', auth()->id())
                ->first();

            $entities->add($entity);
            $entities = $entities->concat($entity->subRecipes);
        }

        return view('recipes/form', ['entities' => $entities]);
    }

    public function postEditRecipe(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $parent = null;
            $ids = [];

            foreach (request()->get('recipe', []) as $recipe)
            {
                $entity = Recipe::with(['ingredients', 'steps', 'steps.photo', 'photos', 'photos.photo'])->find(array_get($recipe, 'id', 0));

                if (!is_null($entity) && $entity->user_id != auth()->id())
                    abort(404);

                if (is_null($entity))
                {
                    $entity = new Recipe();
                    $entity->user_id = auth()->id();
                }

                $ids[] = $entity->id;

                $entity->fill(array_only($recipe, ['name', 'description', 'portions', 'preparing_time', 'difficulty', 'category_id', 'estimated_price', 'share_type']));
                $entity->save();

                if (is_null($parent))
                {
                    $parent = $entity->id;
                    RecipeRecipe::query()->where('parent_id', '=', $entity->id)->delete();
                }
                else
                {
                    $recipe = new RecipeRecipe();
                    $recipe->parent_id = $parent;
                    $recipe->recipe_id = $entity->id;
                    $recipe->save();
                }

                RecipeIngredient::saveIngredientsInRecipe($recipe, $entity->id);
                RecipeStep::saveStepsInRecipe($recipe, $entity->id);
                RecipePhoto::savePhotosInRecipe($recipe, $entity->id);
                SharedRecipe::saveRecipe($recipe, $entity->id);
            }

            DB::commit();
        }
        catch (\Exception $e)
        {
            Log::error($e->getMessage());
            DB::rollBack();

            flash()->error(__('Wystąpił błąd podczas zapisywania przepisu. Spróbuj ponownie później'));
            return redirect()->refresh()->withInput();
        }

        return redirect()->to(route('recipes.getEdit', [$id]));
    }

    public function getFavourites($page = 1)
    {
        $page = intval($page);
        $page = $page > 0 ? $page : 1;

        $filters = session()->get('filters@'.self::class, []);

        $query = Recipe::query()->with('user');
        $query->where('user_id', '=', auth()->id());

        $query->whereIn('id', auth()->user()->favourite_list ?? []);

        $count = $query->count();

        $pager = new Pager($count, 10, $page <= 0 ? 1 : $page);

        $pager->setUrl(route('recipes.getFavourites'));

        $rows = $query->limit($pager->getLimit())->offset($pager->getOffset())->orderBy('name')->get();

        $categories = Category::getCategoriesTree();

        return view('recipes/list', ['rows' => $rows, 'pager' => $pager, 'filters' => $filters, 'categories' => $categories, 'deleteFromList' => true, 'sort' => session()->get('sort@'.self::class, ['date_desc'])]);
    }

    public function postSetFavourite($id)
    {
        $user = auth()->user();

        $user->setFavourite($id);

        return response()->json(['success' => true, 'total' => $user->favouritesCount()]);
    }

    public function getDeleteRecipe($id)
    {
        $entity = Recipe::query()->findOrFail($id);

        if ($entity->user_id != auth()->id())
            abort(404);

        return view('recipes/delete', ['entity' => $entity]);
    }

    public function postDeleteRecipe($id)
    {
        $entity = Recipe::query()->findOrFail($id);

        if ($entity->user_id != auth()->id())
            abort(404);

        $entity->delete();

        RecipeRecipe::query()->where('recipe_id', '=', $id)->orWhere('parent_id', '=', $id)->delete();
        RecipeIngredient::query()->where('recipe_id', '=', $id)->delete();
        RecipeStep::query()->where('recipe_id', '=', $id)->delete();
        RecipePhoto::query()->where('recipe_id', '=', $id)->delete();

        return redirect()->to(route('recipes.getList'));
    }

    public function getRecipesListJson(Request $request)
    {
        $response = [
            'results' => []
        ];

        $page = $request->post('page');
        $search = $request->post('search');

        $limit = 50;
        $page = (int)$page > 0 ? $page : 0;
        $offset = ($page - 1 > 0 ? $page - 1 : 0) * $limit;
        $response['totalCount'] = Recipe::query()->where('name', 'LIKE', '%'.$search.'%')->count();
        $results = Recipe::query()->where('name', 'LIKE', '%'.$search.'%')->limit($limit)->offset($offset)->get();
        if(!empty($results))
        {
            foreach($results as $row)
            {
                $response['results'][] = [
                    'id' => $row['id'],
                    'text' => $row['name']
                ];
            }
        }

        $response['pagination']['more'] = $page * $limit < $response['totalCount'];

        return response()->json($response);
    }

    public function postRate($id)
    {
        if (session()->has('recipe.'.$id))
        {
            return response()->json(['success' => false, 'message' => __('Oceniłeś już ten przepis')]);
        }

        $entity = Recipe::query()->findOrFail($id);

        $rate = intval(request()->input('rating', 0));

        if ($rate > 0)
        {
            $entity->rating_count += 1;
            $entity->rating_value += $rate;
            $entity->save();

            session()->put('recipe.'.$id, true);
        }

        return response()->json(['success' => true, 'count' => $entity->rating_count, 'rate' => $entity->getRatingValue()]);
    }

    public function postAddIngredientToShoppingList()
    {
        $ingredient_id = intval(request()->input('ingredient_id', 0));
        $unit_id = intval(request()->input('unit_id', 0));
        $value = floatval(request()->input('value', 0));

        $ingredient = Ingredient::findOrFail($ingredient_id);

        $row = ShoppingList::query()->where('user_id', auth()->user()->id)->where('ingredient_id', $ingredient_id)->first() ?? new ShoppingList();
        $row->unit_id = $unit_id;
        $row->user_id = auth()->user()->id;
        $row->ingredient_id = $ingredient_id;
        $row->total = isset($row->id) && $row->total > 0 ? $row->total + $value : $value;
        $row->save();

        event(new NotificationEvent(__('Składnik <b>%s</b> został dodany do listy w ilości <b>%s</b>', $ingredient->name, $value . ' ' . $ingredient->getBaseAbbreviation($unit_id, $row->total))));

        return response()->json(['success' => true, 'total' => auth()->user()->shoppingListCount()]);
    }

    public function postRemoveIngredientFromShoppingList()
    {
        $ingredient_id = intval(request()->input('ingredient_id', 0));

        $shoppingListPosition = ShoppingList::query('user_id', auth()->user()->id)->where('ingredient_id', $ingredient_id)->firstOrFail();

        $name = $shoppingListPosition->ingredient->name;
        $value = $shoppingListPosition->total;
        $abbreviation = $shoppingListPosition->ingredient->getBaseAbbreviation($shoppingListPosition->unit_id, $shoppingListPosition->total);

        $shoppingListPosition->delete();

        event(new NotificationEvent(__('Składnik <b>%s</b> został usunięty z listy w ilości <b>%s</b>', $name, $value . ' ' . $abbreviation)));

        return response()->json(['success' => true, 'total' => auth()->user()->shoppingListCount()]);
    }

    public function getShoppingList()
    {
        $rows = ShoppingList::query()->where('user_id', auth()->user()->id)->get();

        $results = [];

        foreach ($rows as $row)
        {
            $results[$row->ingredient_id] = [
                'name' => $row->ingredient->name,
                'ingredient_id' => $row->ingredient_id,
                'total' => $row->total,
                'abbreviation' => $row->ingredient->getBaseAbbreviation($row->unit_id, $row->total)
            ];
        }

        return view('recipes/shopping_list', ['rows' => $results]);
    }
}
