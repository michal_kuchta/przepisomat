<?php

namespace App\Http\Controllers;

use App\Helpers\Pager;
use App\Recipe;
use App\SharedRecipe;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex()
    {
        return view('users.index');
    }

    private function getEditRules()
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore(auth()->user()->id)
            ],
            'name' => 'required|max:200',
            'password' => 'required'
        ];
    }

    private function getEditMessages()
    {
        return [
            'email' => [
                'required' => __('Adres e-mail jest wymagany'),
                'email' => __('Niepoprawny adres e-mail'),
                'unique' => __('Ten adres e-mail jest już zajęty')
            ],
            'name' => [
                'required' => __('Imię i nazwisko jest wymagane'),
                'max' => __('Wartość pola Imię i nazwisko jest za długa'),
            ],
            'password.required' => __('Hasło jest wymagane')
        ];
    }

    public function getEdit()
    {
        $user = auth()->user();

        $user->fill(request()->old());

        $model = array_only($user->toArray(), ['email', 'name', 'is_rodo_checked']);

        return view('users.edit', [
            'model' => $model,
            'rules' => $this->getEditRules()
        ]);
    }

    public function postEdit()
    {
        $this->validateWithBag('form', request(), $this->getEditRules(), $this->getEditMessages());

        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('password')];

        if (!auth()->validate($credentials))
        {
            flash()->error(__('Podane hasło jest nieprawidłowe.'));

            return redirect()->refresh();
        }

        $user = auth()->user();
        $user->fill(request()->only(['email', 'name']));
        $user->is_rodo_checked = ($user->is_rodo_checked == true || $user->is_rodo_checked == 'true' || $user->is_rodo_checked == '1') && request()->has('is_rodo_checked') ? 1 : 0;

        if ($user->isDirty('is_rodo_checked') && $user->is_rodo_checked == '0')
        {
            $user->anonymize();
            auth()->logout();
            flash()->success(__('Twoje dane zostały zanonimizowane. Od teraz zalogowanie się na to konto nie będzie możliwe.'));
            return redirect()->to(route('login'));
        }


        $user->save();

        flash()->success(__('Pomyślnie zapisano zmiany'));

        return redirect()->to(route('users.getEdit'));
    }

    public function getPasswordRules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ];
    }

    public function getPasswordMessages()
    {
        return [
            'old_password.required' => __('Musisz podać obecne hasło aby móc je zmienić'),
            'password' => [
                'required' => __('Nowe hasło jest wymagane'),
                'confirmed' => __('Podane hasła nie są takie same'),
            ],
        ];
    }

    public function getPassword()
    {
        return view('users.password', ['rules' => $this->getPasswordRules()]);
    }

    public function postPassword()
    {
        $this->validateWithBag('form', request(), $this->getPasswordRules(), $this->getPasswordMessages());

        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('old_password')];

        if (!auth()->validate($credentials))
        {
            flash()->error(__('Podane hasło jest nieprawidłowe.'));

            return redirect()->refresh();
        }

        $user = auth()->user();
        $user->password = Hash::make(request()->get('password'));
        $user->save();

        flash()->success(__('Twoje hasło zostało pomyślnie zmienione'));

        return redirect()->to(route('users.getPassword'));
    }

    public function getSettings()
    {
        $shareType = auth()->user()->share_type;
        $sharedEmails = auth()->user()->sharedEmails();

        return view('users.settings', ['shareType' => $shareType, 'sharedEmails' => $sharedEmails]);
    }

    public function postSettings()
    {
        $shareType = request()->input('share_type', 0);

        auth()->user()->share_type = $shareType;
        auth()->user()->save();

        $sharedEmails = request()->input('shared_emails', []);

        SharedRecipe::query()->where(function($sql){
            $sql->orWhere('type', SharedRecipe::TYPE_ALL)
                ->orWhere('type', SharedRecipe::TYPE_CUSTOM);
        })->where('object_id', auth()->user()->id)->delete();

        if (!empty($sharedEmails) && $shareType == SharedRecipe::TYPE_CUSTOM)
        {
            foreach ($sharedEmails as $email)
            {
                $m = new SharedRecipe();
                $m->object_id = auth()->user()->id;
                $m->type = SharedRecipe::TYPE_CUSTOM;
                $m->email = $email;
                $m->save();
            }
        }
        elseif ($shareType == SharedRecipe::TYPE_ALL)
        {
            $m = new SharedRecipe();
            $m->object_id = auth()->user()->id;
            $m->type = SharedRecipe::TYPE_ALL;
            $m->save();
        }

        return redirect()->back();
    }

    public function getDelete()
    {
        return view('users.delete');
    }

    public function postDelete()
    {
        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('password')];

        if (!auth()->validate($credentials))
        {
            flash()->error(__('Podane hasło jest nieprawidłowe.'));

            return redirect()->refresh();
        }

        $user = auth()->user();
        $user->anonymize();

        auth()->logout();
        flash()->success(__('Twoje konto zostało usunięte z serwisu. Od teraz zalogowanie się na to konto nie będzie możliwe.'));

        return redirect()->to(route('login'));

    }
}
