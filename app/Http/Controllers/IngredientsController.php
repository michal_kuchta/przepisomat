<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Helpers\Pager;
use App\Ingredient;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;

class IngredientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getIngredientsList($page = 1)
    {
        $filters = session()->get('filters@'.self::class, []);
        $count = Ingredient::query()->count();

        $pager = new Pager($count, 10, $page);
        $pager->setUrl(route('ingredients.getList'));

        $query = Ingredient::query();

        if(Arr::has($filters, 'query') && !empty(Arr::get($filters, 'query', '')))
        {
            $query->where('name', 'LIKE', '%'.Arr::get($filters, 'query').'%');
        }

        $rows = $query->limit($pager->getLimit())->offset($pager->getOffset())->orderBy('name')->get();

        return view('ingredients/list', ['rows' => $rows, 'pager' => $pager, 'filters' => $filters]);
    }

    public function postIngredientsList(Request $request, $page = 1)
    {
        $filters = $request->only(['query']);

        array_filter($filters, function($val, $key){
            return !empty($val);
        }, ARRAY_FILTER_USE_BOTH);

        session()->put('filters@'.self::class, $filters);

        return redirect(route('ingredients.getList'));
    }

    public function getAddIngredient()
    {
        $entity = new Ingredient();
        return view('ingredients/form', ['entity' => $entity]);
    }

    public function postAddIngredient(Request $request)
    {
        if(!$request->has('name') || empty($request->get('name')))
        {
            return redirect()->to(route('ingredients.getAdd'));
        }

        $entity = new Ingredient();
        $entity->fill($request->all());
        $entity->save();

        return redirect()->to(route('ingredients.getList'));
    }

    public function getEditIngredient($id)
    {
        if (auth()->user()->is_admin) abort(403);

        $entity = Ingredient::findOrFail($id);
        return view('ingredients/form', ['entity' => $entity]);
    }

    public function postEditIngredient(Request $request, $id)
    {
        if (auth()->user()->is_admin) abort(403);

        if(!$request->has('name') || empty($request->get('name')))
        {
            return redirect()->to(route('ingredients.getEdit') . "/" . $id);
        }

        $entity = Ingredient::findOrFail($id);
        $entity->fill($request->all());
        $entity->save();

        return redirect()->to(route('ingredients.getList'));
    }

    public function getDeleteIngredient($id)
    {
        if (auth()->user()->is_admin) abort(403);

        $entity = Ingredient::findOrFail($id);
        return view('ingredients/delete', ['entity' => $entity]);
    }

    public function postDeleteIngredient($id)
    {
        if (auth()->user()->is_admin) abort(403);

        $entity = Ingredient::findOrFail($id);
        $entity->delete();

        return redirect()->to(route('ingredients.getList'));
    }

    public function getIngredientsListJson(Request $request)
    {
        $response = [
            'results' => []
        ];

        $page = $request->post('page');
        $search = $request->post('search');
        $selected = explode(',', $request->post('selected', ''));

        $limit = 50;
        $page = (int)$page > 0 ? $page : 0;
        $offset = ($page - 1 > 0 ? $page - 1 : 0) * $limit;
        $response['totalCount'] = Ingredient::query()->where('name', 'LIKE', '%'.$search.'%')->count();

        if (!empty($search))
        {
            $response['results'][] = [
                'id' => 0,
                'text' => $search . __(" (utwórz nowy)")
            ];
        }

        $results = Ingredient::query()->where('name', 'LIKE', '%'.$search.'%')->whereNotIn('id', $selected)->limit($limit)->offset($offset)->get();
        if(!empty($results))
        {
            foreach($results as $row)
            {
                $response['results'][] = [
                    'id' => $row['id'],
                    'text' => $row['name']
                ];
            }
        }

        $response['pagination']['more'] = $page * $limit < $response['totalCount'];

        return response()->json($response);
    }

    public function postCreateIngredient()
    {
        $name = Helpers::mb_replace(__(" (utwórz nowy)"), '', request()->input('name'));

        $ingredient = new Ingredient();
        $ingredient->name = $name;
        $ingredient->save();

        return response()->json([
            'id' => $ingredient->id,
            'text' => $ingredient->name
        ]);
    }
}
