<?php

namespace App;

use App\Helpers\Eloquent\Relations\HasRelations;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Recipe[] $recipes
 * @property-read int|null $recipes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable, HasRelations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_rodo_checked', 'share_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_rodo_checked' => 'integer',
        'favourite_list' => 'array'
    ];

    public function favourites()
    {
        $this->hasManyWithArray(Recipe::class, 'id', 'favourite_list');
    }

    public function recipes()
    {
        return $this->hasMany(Recipe::class, 'user_id', 'id');
    }

    public function anonymize()
    {
        $this->is_active = false;
        $this->is_admin = false;
        $this->is_rodo_checked = false;
        $this->email = 'user' . $this->id . '@anonymized.xyz';
        $this->password = Hash::make(str_random(10));
        $this->save();

        $recipesIds = $this->recipes->pluck('id');

        RecipeIngredient::whereIn('recipe_id', $recipesIds)->delete();
        RecipePhoto::whereIn('recipe_id', $recipesIds)->delete();
        RecipeStep::whereIn('recipe_id', $recipesIds)->delete();
        RecipeRecipe::orWhereIn('recipe_id', $recipesIds)->orWhereIn('parent_id', $recipesIds)->delete();
    }

    public function setFavourite($id)
    {
        Recipe::findOrFail($id);

        if (!is_array($this->favourite_list))
            $this->favourite_list = [];

        if (in_array($id, $this->favourite_list))
        {
            $list = $this->favourite_list;
            unset($list[array_search($id, $list)]);
            $this->favourite_list = $list;
        }
        else
        {
            $this->favourite_list = array_merge($this->favourite_list, [$id]);
        }
        $this->save();
    }

    public function favouritesCount()
    {
        return is_array($this->favourite_list) ? count($this->favourite_list) : 0;
    }

    public function shoppingListCount()
    {
        return ShoppingList::query()->where('user_id', auth()->user()->id)->count();
    }

    public function sharedEmails()
    {
        return SharedRecipe::query()->where('type', SharedRecipe::TYPE_CUSTOM)->where('object_id', $this->id)->pluck('email')->toArray();
    }

    public function getSharedUserIds()
    {
        $userIds = SharedRecipe::query()
            ->orWhere(function ($sql){
                $sql->where('email', auth()->user()->email)
                    ->where('type', SharedRecipe::TYPE_CUSTOM);
            })
            ->orWhere('type', SharedRecipe::TYPE_ALL)
            ->pluck('object_id')
            ->toArray();

        $userIds[] = auth()->user()->id;

        return $userIds;
    }

    public function getSharedRecipesIds()
    {
        return SharedRecipe::query()
            ->orWhere(function ($sql){
                $sql->where('email', auth()->user()->email)
                    ->where('type', SharedRecipe::TYPE_RECIPE_CUSTOM);
            })
            ->orWhere('type', SharedRecipe::TYPE_RECIPE_ALL)
            ->pluck('object_id')
            ->toArray();
    }
}
