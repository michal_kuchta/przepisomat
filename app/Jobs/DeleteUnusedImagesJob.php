<?php

namespace App\Jobs;

use App\Photo;
use App\RecipePhoto;
use App\RecipeStep;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteUnusedImagesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('other');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->removeNotExistingFilesFromDatabase();

        $this->removeNotUsedFromDatabase();
    }

    private function removeNotExistingFilesFromDatabase()
    {
        $count = Photo::count();
        $limit = 1000;

        $iterations = ceil($count / 1000);

        for ($i = $iterations; $i >= 1; $i--)
        {
            $rows = Photo::query()->limit($limit)->offset(($i-1) * $limit)->get();

            if ($rows->count() > 0)
            {
                /**
                 * @var Photo $row
                 */
                foreach ($rows as $row)
                {
                    if (!file_exists(public_path($row->url)))
                    {
                        $row->delete();
                    }
                }
            }
        }
    }

    private function removeNotUsedFromDatabase()
    {
        $recipe_steps = RecipeStep::pluck('photo_id')->toArray();
        $recipe_photos = RecipePhoto::pluck('photo_id')->toArray();

        $ids = array_merge($recipe_photos, $recipe_steps);
        array_unique($ids);

        $rows = Photo::whereNotIn('id', $ids)->get();
        /**
         * @var Photo $row
         */
        foreach ($rows as $row)
        {
            if (file_exists(public_path($row->url)))
            {
                $row->delete();
                unlink(public_path($row->url));
            }
        }
    }
}
