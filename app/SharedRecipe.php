<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedRecipe extends Model
{
    const TYPE_NONE = 0;
    const TYPE_CUSTOM = 1;
    const TYPE_ALL = 2;
    const TYPE_RECIPE_CUSTOM = 3;
    const TYPE_RECIPE_ALL = 4;

    protected $table = 'shared_recipes';
    public $timestamps = false;
    protected $fillable = [
        'email',
        'type',
        'object_id',
    ];

    public static function saveRecipe($recipe, $recipe_id)
    {
        $shareType = array_get($recipe, 'share_type', self::TYPE_NONE);

        $sharedEmails = array_get($recipe, 'shared_emails', []);

        SharedRecipe::query()->where(function($sql){
            $sql->orWhere('type', SharedRecipe::TYPE_RECIPE_ALL)
                ->orWhere('type', SharedRecipe::TYPE_RECIPE_CUSTOM);
        })->where('object_id', $recipe_id)->delete();

        if (!empty($sharedEmails) && $shareType == SharedRecipe::TYPE_RECIPE_CUSTOM)
        {
            foreach ($sharedEmails as $email)
            {
                $m = new SharedRecipe();
                $m->object_id = $recipe_id;
                $m->type = SharedRecipe::TYPE_RECIPE_CUSTOM;
                $m->email = $email;
                $m->save();
            }
        }
        elseif ($shareType == SharedRecipe::TYPE_RECIPE_ALL)
        {
            $m = new SharedRecipe();
            $m->object_id = $recipe_id;
            $m->type = SharedRecipe::TYPE_RECIPE_ALL;
            $m->save();
        }
    }

}
