<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\RecipePhoto
 *
 * @property int $recipe_id
 * @property int $photo_id
 * @property int $id
 * @property-read \App\Photo $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipePhoto whereRecipeId($value)
 * @mixin \Eloquent
 */
class RecipePhoto extends Model
{
    protected $table = 'recipe_photos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'recipe_id',
        'photo_id',
    ];

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public static function savePhotosInRecipe($recipe, $recipeId)
    {
        RecipePhoto::query()->where('recipe_id', '=', $recipeId)->delete();
        if(array_has($recipe, 'images') && count(array_get($recipe, 'images')) > 0)
        {
            foreach(array_get($recipe, 'images') as $image)
            {
                $entity = new RecipePhoto();
                $entity->recipe_id = $recipeId;
                $entity->photo_id = $image;
                $entity->save();
            }
        }
    }
}
