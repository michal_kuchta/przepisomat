<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeRecipe extends Model
{
    protected $table = 'recipe_recipes';
    public $timestamps = false;
    protected $fillable = [
        'recipe_id',
        'parent_id'
    ];
}
