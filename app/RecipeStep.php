<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\RecipeStep
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $recipe_id
 * @property int $position
 * @property string|null $description
 * @property int|null $photo_id
 * @property-read \App\Photo $photo
 * @property-read \App\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeStep whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RecipeStep extends Model
{
    protected $table = 'recipe_steps';
    public $timestamps = true;
    protected $fillable = [
        'recipe_id',
        'position',
        'description',
        'image_id'
    ];

    public function recipe()
    {
        return $this->belongsTo(Recipe::class, 'recipe_id', 'id');
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public static function saveStepsInRecipe($recipe, $recipeId)
    {
        RecipeStep::query()->where('recipe_id', '=', $recipeId)->delete();
        if(array_has($recipe, 'step') && count(array_get($recipe, 'step', [])) > 0)
        {
            foreach(array_get($recipe, 'step') as $stepId => $stepData)
            {
                $entity = new RecipeStep();
                $entity->position = $stepId;
                $entity->description = $stepData['description'];
                $entity->recipe_id = $recipeId;
                if(isset($stepData['photo']) && !empty($stepData['photo']))
                {
                    $entity->photo_id = $stepData['photo'];
                }

                $entity->save();
            }
        }
    }
}
