<?php

namespace App\Listeners;

use Illuminate\Console\Events\CommandFinished;
use Illuminate\Support\Facades\Artisan;

class CommandFinishedListener
{
    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Registered  $event
     * @return void
     */
    public function handle(CommandFinished $event)
    {
        if ($event->command == 'migrate' || $event->command == 'migrate:rollback')
        {
            Artisan::call('ide-helper:generate', ['--no-interaction' => true]);
            echo "Ide helper generated\n";
            Artisan::call('ide-helper:models', ['--no-interaction' => true]);
            echo "Ide helper models generated\n";
            Artisan::call('ide-helper:meta', ['--no-interaction' => true]);
            echo "Ide helper meta generated\n";
        }
    }
}