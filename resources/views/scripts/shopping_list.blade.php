<script>

    var ShoppingList = {
        Init: function(){
            $('.add-to-cart-btn').on('click', function(){
                var el = $(this);
                var unit_id = $(el).attr('data-unit');
                var ingredient_id = $(el).attr('data-ingredient');
                var value = $(el).attr('data-value');
                var body = {
                    ingredient_id: ingredient_id,
                    value: value,
                    unit_id: unit_id
                };

                var urlAdd = "{{ route('recipes.postAddIngredientToShoppingList') }}";
                var urlRemove = "{{ route('recipes.postRemoveIngredientFromShoppingList') }}";

                $.post($(el).hasClass('remove-row') ? urlRemove : urlAdd, body, function(data){
                    if (data.success)
                    {
                        $('#shopping-list-counter').html(data.total);

                        if ($(el).hasClass('remove-row'))
                        {
                            $(el).closest('.row').remove();
                        }
                    }
                });
            });
        }
    };

    $(document).ready(function () {
        ShoppingList.Init();
    });
</script>