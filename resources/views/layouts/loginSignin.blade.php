<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('partials/head')
</head>
<body>
    <div class="container">
        <div class="row vh-100 align-items-center">
            <div class="col-lg-4 col-md-8 col-sm-12 m-auto text-center">
                <div class="logo">
                    <img src="/images/logo-small.png " />
                </div>
                @yield('header')

                @include('partials.alert.display')

                @yield('content')
            </div>
        </div>
    </div>

</body>
</html>
