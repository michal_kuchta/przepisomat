@extends('layouts.main')

@section('content')
    <div class="sidepanel-wrap">
        <div class="sidepanel" data-spy="sticky" data-offset-top="66" data-offset-bottom="70" data-min-width="1200">
            @yield('sidepanel')
        </div>
        <div class="sidepanel-content">
            @yield('panel')
        </div>
    </div>
@endsection
