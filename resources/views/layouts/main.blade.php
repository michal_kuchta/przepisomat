<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.head')
</head>
<body>
    <div id="app">
        <div class="">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm mt-lg-4 mt-md-3 mt-sm-0 pt-0 pb-0">
                    <a class="navbar-brand" href="{{ route('recipes.getList') }}">
                        <img class="d-lg-block" style="height: 55px" src="/images/logo.png">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <form method="POST" class="ml-auto w-50" action="{{ route('postHome') }}">
                            @csrf
                            <input type="text" name="query" placeholder="{{ __('wyszukaj') }}" class="form-control w-75 d-inline" value="{{ isset($filters) ? Arr::get($filters, 'query', '') : '' }}" />
                            <input type="submit" value="{{ __('Szukaj') }}" class="btn btn-primary">
                            @if(isset($filters) && Arr::has($filters, 'query') && !empty(Arr::get($filters, 'query')))
                                <button class="btn btn-danger clear_button">X</button>
                                <script>
                                    $(document).ready(function () {
                                        $('.clear_button').on('click', function () {
                                            $('input[name="query"]').val('');
                                            $('input[type="submit"]').trigger('click');
                                        });
                                    });
                                </script>
                            @endif
                        </form>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav main-menu">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @else
                                <li class="nav-item dropdown">
                                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('home') || request()->routeIs('home1') || (request()->routeIs('recipes.*') && !request()->routeIs('recipes.getFavourites')) ? ' active' : '' }}" href="{{ url('/') }}">{{ __('Przepisy') }}</a>
                                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('ingredients.getList') ? ' active' : '' }}" href="{{ route('ingredients.getList') }}">{{ __('Składniki') }}</a>
                                    <a class="position-relative nav-link d-lg-inline d-sm-block{{ request()->routeIs('recipes.getFavourites') ? ' active' : '' }}" href="{{ route('recipes.getFavourites') }}">
                                        <span class="d-lg-none d-sm-inline"> {{ __('Ulubione') }}</span>
                                        <i class="d-lg-inline d-sm-none fas fa-heart" title="{{ __('Ulubione') }}" data-toggle="tooltip" data-placement="bottom"></i>
                                        <span class="d-lg-inline d-sm-none" id="favourite-counter">{{ auth()->user()->favouritesCount() }}</span>
                                    </a>
                                    <a class="position-relative nav-link d-lg-inline d-sm-block{{ request()->routeIs('recipes.getShoppingList') ? ' active' : '' }}" href="{{ route('recipes.getShoppingList') }}">
                                        <span class="d-lg-none d-sm-inline"> {{ __('Lista zakupów') }}</span>
                                        <i class="d-lg-inline d-sm-none fas fa-shopping-cart" title="{{ __('Lista zakupów') }}" data-toggle="tooltip" data-placement="bottom"></i>
                                        <span class="d-lg-inline d-sm-none" id="shopping-list-counter">{{ auth()->user()->shoppingListCount() }}</span>
                                    </a>
                                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('users.getIndex') ? ' active' : '' }}" href="{{ route('users.getIndex') }}">
                                        <span class="d-lg-none d-sm-inline"> {{ __('Moje konto') }}</span>
                                        <i class="d-lg-inline d-sm-none fas fa-user-cog" title="{{ __('Moje konto') }}" data-toggle="tooltip" data-placement="bottom"></i>
                                    </a>
                                    <a class="nav-link d-lg-inline d-sm-block" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <span class="d-lg-none d-sm-inline"> {{ __('Wyloguj się') }}</span>
                                        <i class="d-lg-inline d-sm-none fas fa-power-off" title="{{ __('Wyloguj się') }}" data-toggle="tooltip" data-placement="bottom"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        @if(auth()->user()->is_admin == 1)
            @include('admin.partials.menu')
        @endif

        @yield('topPanel')

        <main class="py-4">
            <div class="container">
                @yield('topContent')

                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body text-center">
                                {!! __('Przepisomat &copy; 2020') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        @php
            $notifications = [];
            foreach (session('flash_notification', collect())->toArray() as $message)
            {
                $notifications[] = [
                    'type' => $message['level'],
                    'content' => $message['message']
                ];
            }
            {{ session()->forget('flash_notification'); }}
        @endphp
        <notifications-component
            :notifications="{{ json_encode($notifications) }}"
            user-id="{{ auth()->user()->id ?? 0 }}"
        ></notifications-component>
    </div>

    @yield('scripts', '')
</body>
</html>