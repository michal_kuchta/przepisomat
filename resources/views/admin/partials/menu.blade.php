<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm mt-lg-4 mt-3 pt-0 pb-0" style="min-height: 50px;">
        <span class="font-weight-bold">{{ __('Panel administratora') }}</span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAdminMenu" aria-controls="navbarAdminMenu" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarAdminMenu">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav w-100 text-right">
                <li class="nav-item dropdown w-100">
                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('admin.dashboard*') ? ' active' : '' }}" href="{{ route('admin.dashboard.getIndex') }}">{{ __('Strona główna') }}</a>
                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('admin.users*') ? ' active' : '' }}" href="{{ route('admin.users.getIndex') }}">{{ __('Użytkownicy') }}</a>
                    <a class="nav-link d-lg-inline d-sm-block{{ request()->routeIs('admin.categories*') ? ' active' : '' }}" href="{{ route('admin.categories.getIndex') }}">{{ __('Kategorie') }}</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
