<ul class="dropdown-menu popover-menu" data-id="{{ $model->id }}">
    <li>
        <a href="{{ route('admin.categories.getEdit', [$model->id]) }}">
            <i class="fa fa-fw fa-pencil"></i>
{{ __('Edycja') }}
</a>
</li>
<li>
    <a href="{{ route('admin.categories.getCreate', [$model->id]) }}">
        <i class="fa fa-fw fa-plus"></i>
        {{ __('Dodaj podkategorię') }}
    </a>
</li>
<li class="divider"></li>
<li>
    <a href="{{ route('admin.categories.getDelete', [$model->id]) }}">
        <i class="fa fa-fw fa-trash text-danger"></i>
        <span class="text-danger">{{ __('Usuń kategorię') }}</span>
    </a>
</li>
</ul>
