@extends('admin.categories.layout')

@section('panel')
    {!! FluentForm::horizontal($model)->rules($rules) !!}

    {!! FluentForm::group()->text('name')->label(__('Nazwa kategorii')) !!}
    {!! FluentForm::group()->checkbox('is_active')->label(__('Aktywny')) !!}
    {!! FluentForm::group()->select('parent_id', $categories)->label(__('Kategoria nadrzędna'))->placeholder(__('Brak kategorii nadrzędnej')) !!}

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'), 'save'),
        Fluent::link(route('admin.categories.getIndex'), __('Powrót'))->addClass('btn btn-danger')
    ]) !!}

    {!! FluentForm::close() !!}
@endsection

@section('scripts')
    @parent
@endsection
