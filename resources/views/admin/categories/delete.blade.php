@extends('admin.categories.layout')

@section('panel')
    <h2 class="header">{{ __('Czy jesteś pewien, że chcesz usunąć kategorię oraz wszystkie przypisane podkategorie?') }}</h2>
    {!! FluentForm::horizontal() !!}
    {!! FluentForm::footer([
        FluentForm::submit('save', __('Tak, usuń'), 'save')->addClass('btn btn-danger'),
        Fluent::link(route('admin.categories.getIndex'), __('Nie, powrót'))
    ]) !!}

    {!! FluentForm::close() !!}
@endsection

@section('scripts')
    @parent
@endsection