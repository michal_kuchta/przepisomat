<li data-id="{{ $item->id }}">
    @if($item->has_children == true && in_array($item->id, $expanded))
        <a href="{{ route('admin.categories.postGetTree', [$item->id]) }}" class="node-sign far fa-minus-square" data-tree="expanded"></a>
        <span class="node-icon fa fa-fw fa-folder-open"></span>
        @if($advanced)
            <i class="node-icon node-icon-sort fa fa-bars fa-pull-right"></i>
            <i class="node-icon node-icon-menu fa fa-cog fa-pull-right" data-tree-menu="{{ $item->id }}"></i>
        @endif
        @include('admin.categories.tree-url', ['model' => $model, 'item' => $item])
        @include('admin.categories.tree-nodes', ['model' => $model, 'parent_id' => $item->id])
    @elseif($item->has_children)
        <a href="{{ route('admin.categories.postGetTree', [$item->id]) }}" class="node-sign far fa-plus-square" data-tree="load"></a>
        <span class="node-icon fa fa-fw fa-folder"></span>
        @if($advanced)
            <i class="node-icon node-icon-sort fa fa-bars fa-pull-right"></i>
            <i class="node-icon node-icon-menu fa fa-cog fa-pull-right" data-tree-menu="{{ $item->id }}"></i>
        @endif
        @include('admin.categories.tree-url', ['model' => $model, 'item' => $item])
    @else
        <span class="node-icon fa fa-fw fa-file-o"></span>
        @if($advanced)
            <i class="node-icon node-icon-sort fa fa-bars fa-pull-right"></i>
            <i class="node-icon node-icon-menu fa fa-cog fa-pull-right" data-tree-menu="{{ $item->id }}"></i>
        @endif
        @include('admin.categories.tree-url', ['model' => $model, 'item' => $item])
    @endif
</li>
