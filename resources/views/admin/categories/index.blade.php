@extends('admin.categories.layout')

@section('panel')
    <div class="alert alert-warning">
        {{ __('Nie wybrano żadnej kategorii.') }}
    </div>
@endsection


@section('scripts')
    @parent
@endsection

