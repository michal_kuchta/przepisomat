@if($currentCategoryId == $item->id)
    <a class="node-text {{ $item->is_active ? '' : 'text-muted' }}" href="{{ route('admin.categories.getEdit', [$item->id]) }}"><b>{{ $item->name }}</b></a>
@else
    <a class="node-text {{ $item->is_active ? '' : 'text-muted' }}" href="{{ route('admin.categories.getEdit', [$item->id]) }}"><span>{{ $item->name }}</span></a>
@endif
