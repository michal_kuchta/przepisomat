@if($model->count() > 0)
    <ul class="tree" data-tree-advanced="{{ $advanced }}">
        <li class="root">
            @include('admin.categories.tree-nodes', ['model' => $model, 'parent_id' => null])
        </li>
    </ul>
@else
    <div class="alert alert-warning">
        {!! __('<a href="%s">Kliknij tutaj</a> aby dodać kategorie do tego katalogu.', route('admin.categories.getCreate')) !!}
    </div>
@endif
