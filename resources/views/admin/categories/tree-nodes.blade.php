<ul data-parent-id="{{ $parent_id }}"  class="sortable">
    @foreach($model->where('parent_id', '==', $parent_id) as $item)
        @include('admin.categories.tree-node', ['model' => $model, 'item' => $item])
    @endforeach
</ul>
