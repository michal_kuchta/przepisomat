@extends('layouts.sidemenu')


@section('sidepanel')
    <ul class="list-group">
        <li class="list-group-item clearfix">
            <b>{{ __('Kategorie') }}</b>
            <div class="btn-group pull-right">
                <a href="{{ route('admin.categories.getCreate') }}" class="btn btn-xs btn-default" title="{{ __('Dodaj kategorię') }}">
                    <span class="fa fa-fw fa-plus"></span>
                </a>
            </div>
        </li>
        <li class="list-group-item">
            @include('admin.categories.tree', ['model' => $categories])
        </li>
    </ul>
@endsection

@section('scripts')
    <script type="text/javascript">
        var initSortableNodes = function(selector)
        {
            $(selector).sortable({
                distance: 5,
                items: "> li",
                handle: ".node-icon.fa-bars",
                update: function (event, ui)
                {
                    var data = {
                        parent: $(this).data('parent-id'),
                        positions: $(this).find("> li")
                            .map(function ()
                            {
                                return $(this).data("id");
                            })
                            .get()
                    };

                    $.post("{{ route('admin.categories.postSortMenu') }}", data);
                }
            });
        };

        $(document).ready(function(){
            initSortableNodes("ul.sortable");
            $('.tree').on('click', '[data-tree]', function(e)
            {
                e.preventDefault();

                console.log('asfsf');
                var $this = $(this);
                var $parent = $this.parent();

                var $ajax = $.post($this.attr('href'),
                    {
                        mode: $this.data('tree'),
                        advanced: $('.tree').data('tree-advanced')
                    });

                if ($this.data('tree') == 'load')
                {
                    $ajax.done(function (result)
                    {
                        $this.removeClass('fa-plus-square').addClass('fa-minus-square').data('tree', 'expanded');
                        $parent.find('> .fa-folder.node-icon').removeClass('fa-folder').addClass('fa-folder-open');
                        $parent.append(result);

                        initSortableNodes('li[data-id="' + $this.parent().data('id') + '"] ul.sortable');
                    });
                }
                else if ($this.data('tree') == 'expanded')
                {
                    $ajax.done(function (result)
                    {
                        $this.removeClass('fa-minus-square').addClass('fa-plus-square').data('tree', 'collapsed');
                        $parent.find('> .fa-folder-open.node-icon').removeClass('fa-folder-open').addClass('fa-folder');
                        $parent.find('> ul').hide();
                    });
                }
                else if ($this.data('tree') == 'collapsed')
                {
                    $ajax.done(function (result)
                    {
                        $this.removeClass('fa-plus-square').addClass('fa-minus-square').data('tree', 'expanded');
                        $parent.find('> .fa-folder.node-icon').removeClass('fa-folder').addClass('fa-folder-open');
                        $parent.find('> ul').show();
                    });
                }

                return false;
            });
            $('.tree').on('click', '[data-tree-menu]', function()
            {
                var data = {
                    id: $(this).data("tree-menu")
                };

                $('[data-tree-menu]').popover('hide');

                var $this = $(this)
                    .addClass('fa-spin')
                    .addClass('node-icon-menu-active');


                $.post("{{ route('admin.categories.postGetTreeMenu') }}", data).done(function(result)
                {
                    $this
                        .popover({
                            html: true,
                            container: 'body',
                            placement: 'left',
                            animation: false,
                            template: '<div class="popover" data-toggle="tooltip"><div class="arrow"></div><div class="popover-content p-a-0"></div></div>',
                            content: result
                        })
                        .popover('show')
                        .removeClass('fa-spin')
                        .removeClass('node-icon-menu-active');
                });
            });
        });
    </script>
@endsection
