@extends('layouts.main')

@section('content')
    <h2 class="header">{{ __('Czy jesteś pewien, że chcesz usunąć użytkownika') }}</h2>
    {!! FluentForm::horizontal() !!}
    <div class="row mt-2 mb-2">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-6">{{ __('Imię i Nazwisko') }}</div>
                <div class="col-6">{{ $model->name }}</div>
            </div>
            <div class="row">
                <div class="col-6">{{ __('E-mail') }}</div>
                <div class="col-6">{{ $model->email }}</div>
            </div>
            <div class="row">
                <div class="col-6">{{ __('Aktywny') }}</div>
                <div class="col-6">{{ $model->is_active == '1' ? __('TAK') : __('NIE') }}</div>
            </div>
        </div>
    </div>

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Tak, usuń'), 'save')->addClass('btn btn-danger'),
        Fluent::link(route('admin.users.getIndex'), __('Nie, powrót'))
    ]) !!}

    {!! FluentForm::close() !!}
@endsection
