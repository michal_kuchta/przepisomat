@extends('layouts.main')

@section('content')
    <h2 class="header">{{ __('Edycja użytkownika') }}</h2>
    {!! FluentForm::horizontal($model)->rules($rules) !!}

    {!! FluentForm::group()->text('name')->label(__('Imię i Nazwisko')) !!}
    {!! FluentForm::group()->text('email')->label(__('E-mail')) !!}
    {!! FluentForm::group()->checkbox('is_active')->label(__('Aktywny')) !!}

    @if(empty($model->id))
        {!! FluentForm::group()->password('password')->label(__('Hasło')) !!}
    @endif

    {!! FluentForm::footer([
        FluentForm::submit('save', __('Zapisz'), 'save'),
        Fluent::link(route('admin.users.getIndex'), __('Powrót'))->addClass('btn btn-danger')
    ]) !!}

    {!! FluentForm::close() !!}
@endsection
