@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{ route('admin.users.getCreate') }}" class="btn btn-success">{{ __('Dodaj użytkownika') }}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-1">{{ __('#') }}</div>
        <div class="col-4">{{ __('Email') }}</div>
        <div class="col-4">{{ __('Nazwa') }}</div>
        <div class="col-3"></div>
    </div>

    @if($rows && count($rows) > 0)
        @foreach($rows as $row)
            <hr />
            <div class="row">
                <div class="col-1">{{ $row->id }}</div>
                <div class="col-4">{{ $row->email }}</div>
                <div class="col-4">{{ $row->name }}</div>
                <div class="col-3 text-right">
                    {!! FluentForm::horizontal()->url(route('admin.users.postActivate', [$row->id])) !!}
                        {!! FluentForm::submit('activate', $row->is_active ? __('Dezaktywuj') : __('Aktywuj'))->css('btn', 'btn-warning') !!}
                        {!! Fluent::link(route('admin.users.getEdit', [$row->id]), __('Edytuj'))->css('btn', 'btn-primary') !!}
                        {!! Fluent::link(route('admin.users.getDelete', [$row->id]), __('Usuń'))->css('btn', 'btn-danger') !!}
                    {!! FluentForm::close() !!}
                </div>
            </div>
        @endforeach
    @else
        <div class="row">
            <div class="col-12">
                <h3>{{ __('Brak danych') }}</h3>
            </div>
        </div>
    @endif

    {!! $pager !!}
@endsection
