@extends('layouts.loginSignin')

@section('header')
    <header>
        <h1>{{ __('Zaloguj się') }}</h1>
    </header>
@endsection

@section('content')
    <div class="col-11 m-auto">
        <form action="{{ route('login') }}" method="POST">
            @csrf
            <input type="text" name="email" placeholder="{{ __('Adres e-mail') }}" class="form-control mb-2" />
            <input type="password" id="password" name="password" placeholder="{{ __('Hasło') }}" class="form-control mb-2" />
            <input type="checkbox" name="remember">{!! __('Zapamiętaj mnie') !!}<br>
            <button type="submit" class="btn btn-primary w-100 mb-2" >{{ __('Zaloguj') }}</button>
            <a href="{{ route('register') }}" class="btn btn-primary w-100" >{{ __('Załóż konto') }}</a>
        </form>
    </div>

    <script>
        $('document').ready(function(){
            $('input[name="username"]').focus();
        });
    </script>
@endsection
