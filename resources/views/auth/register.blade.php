@extends('layouts.loginSignin')

@section('header')
    <header>
        <h1>{{ __('Zarejestruj się') }}</h1>
    </header>
@endsection

@section('content')
    <div class="col-11 m-auto">
        {!! FluentForm::standard()->url(route('register'))->rules(['is_rodo_checked' => 'required', 'name' => 'required', 'email' => 'required|email'])->method('POST') !!}
            @csrf
            <input type="text" name="email" placeholder="{{ __('Adres e-mail') }}" class="form-control mb-2" />

            <input type="text" name="name" placeholder="{{ __('Imię i Nazwisko') }}" class="form-control mb-2" />
            <input type="password" id="password" name="password" placeholder="{{ __('Hasło') }}" class="form-control mb-2" />
            <input type="password" id="repeat_password" name="password_confirmation" placeholder="{{ __('Powtórz hasło') }}" class="form-control mb-2" />

            @include('partials.rodo')

            <button type="submit" class="btn btn-primary w-100 mb-2" >{{ __('Załóż konto') }}</button>
            <a href="{{ route('login') }}" class="btn btn-primary w-100" >{{ __('Zaloguj') }}</a>
        {!! FluentForm::close() !!}
    </div>

    <script>
        $('document').ready(function(){
            $('input[name="username"]').focus();
        });
    </script>
@endsection
