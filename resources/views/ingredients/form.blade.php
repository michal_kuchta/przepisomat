@extends('layouts.main')

@section('content')
    <form method="POST">
        @csrf
        <div class="form-group">
            <label for="name">{{ __('Nazwa') }}</label>
            <input type="text" class="form-control" name="name" value="{{ $entity->name }}">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="{{ __('Zapisz') }}" />
            <a class="btn btn-danger" href="{{ route('ingredients.getList') }}">{{ __('Powrót') }}</a>
        </div>
    </form>
@endsection
