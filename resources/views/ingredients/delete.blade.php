@extends('layouts.main')

@section('content')
    <form method="POST">
        @csrf
        <div class="form-group">
            <label>{{ __('Nazwa') }}: {{ $entity->name }}</label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-danger" value="{{ __('Usuń') }}" />
            <a href="{{ route('ingredients.getList') }}" class="btn btn-success">{{ __('Powrót') }}</a>
        </div>
    </form>
@endsection
