@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-4 text-left">
            <a class="btn btn-success" href="{{ route('ingredients.getAdd') }}">{{ __('Dodaj składnik') }}</a>
        </div>
        <div class="col-lg-8 text-right">
            <form method="POST">
                @csrf
                <input type="text" name="query" placeholder="{{ __('wyszukaj') }}" class="form-control w-50 d-inline" value="{{ Arr::get($filters, 'query', '') }}" />
                <input type="submit" value="{{ __('Szukaj') }}" class="btn btn-primary">
                @if(Arr::has($filters, 'query') && !empty(Arr::get($filters, 'query')))
                    <button class="btn btn-danger clear_button">{{ __('Wyczyść') }}</button>
                    <script>
                        $(document).ready(function () {
                            $('.clear_button').on('click', function () {
                                $('input[name="query"]').val('');
                                $('input[type="submit"]').trigger('click');
                            });
                        });
                    </script>
                @endif
            </form>
        </div>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">{{ __('Nazwa') }}</th>
            <th scope="col" style="text-align: right;">{{ __('Akcje') }}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($rows as $id => $row)
                <tr>
                    <td>{{ $row->name }}</td>
                    <td style="text-align: right;">
                        @if (auth()->user()->is_admin)
                        <a href="{{ route('ingredients.getEdit', [$row->id]) }}" class="btn btn-success mb-2 mb-lg-0 pl-2">{{ __('Edytuj') }}</a>
                        <a href="{{ route('ingredients.getDelete', [$row->id]) }}" class="btn btn-danger">{{ __('Usuń') }}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {!! $pager !!}
@endsection
