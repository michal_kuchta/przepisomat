<div class="card mt-3 tab-card recipe-row recipe-row-{{ $id }}" data-id="{{ $id }}">
    <div class="tab-pane fade show active p-3" id="general-{{ $id }}" role="tabpanel" aria-labelledby="general-tab-{{ $id }}">
        <div class="text-right">
            <span class="btn btn-danger deleteRecipe">X</span>
        </div>
        <div class="form-group">
            <label for="nazwa">{{ __('Wybierz przepis do podpięcia') }}</label><br>
            <select class="form-control w-50 d-inline jqAddExistingRecipe" style="width: 50%" data-selected="" recipe-id="{{ $id }}"></select>
        </div>
    </div>
</div>
