@if($entity->user_id === auth()->id() && !isset($secondLevel))
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{ route('recipes.getEdit', [$entity->id]) }}" class="btn btn-warning">{{ __('Edytuj') }}</a>
            <a href="{{ route('recipes.getDelete', [$entity->id]) }}" class="btn btn-danger">{{ __('Usuń') }}</a>
        </div>
    </div>
@endif
<div class="row">
    @if(!isset($secondLevel) && count($entity->getAllPhotos()) > 0)
        <div class="col-lg-6">
            <div id="carousel_recipe" class="carousel slide ml-auto mr-auto" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($entity->getAllPhotos() as $id => $photo)
                        <li data-target="#carousel_recipe" data-slide-to="{{ $id }}" class="{{ $id == 0 ? 'active' : '' }}"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach($entity->getAllPhotos() as $id => $photo)
                        <div class="carousel-item {{ $id == 0 ? 'active' : '' }}">
                            <img class="d-block ml-auto mr-auto" style="width: 100%;" src="{{ $photo->photo->url }}">
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carousel_recipe" role="button" data-slide="prev">
                    <span class="left-arrow" aria-hidden="true"></span>
                    <span class="sr-only">{{ __('Poprzednie') }}</span>
                </a>
                <a class="carousel-control-next" href="#carousel_recipe" role="button" data-slide="next">
                    <span class="right-arrow" aria-hidden="true"></span>
                    <span class="sr-only">{{ __('Następne') }}</span>
                </a>
            </div>
        </div>
    @endif

    <div class="col-lg-{{ !isset($secondLevel) ? 6 : 12 }}">
        <h3>{{ $entity->name }}</h3>

        @if(!isset($secondLevel))
            @foreach($entity->getAllIngredients() as $ingredient)
                <div class="row">
                    <div class="col-4">{{ $ingredient->ingredient->name }}</div>
                    <div class="col-8">{{ $ingredient->getValue(true) }}{{ $ingredient->getBaseUnit()['abbreviation'] }}</div>
                </div>
            @endforeach
        @endif
    </div>
</div>

@if(!empty($entity->description))
    <div class="row">
        <div class="col-lg-12">
            <h1 class="header">{{ __('Opis') }}</h1>
            {!! $entity->description !!}
        </div>
    </div>
@endif

@if(!empty($entity->steps))
    <div class="row">
        <div class="col-lg-12">
            @foreach($entity->steps as $step)
                <h3 class="header mt-3">{{ __('Krok: ') }}{{ $step->position }}</h3>
                <div class="row">
                    @if($step->photo) <div class="col-lg-4"><img src="{{ $step->photo->url }}" style="max-width: 100%; max-height: 500px"></div> @endif
                    <div class="col-lg-8">{{ $step->description }}</div>
                </div>
            @endforeach
        </div>
    </div>
@endif