<div class="row @if(!isset($ingredient)) ingredientTemplate @else ingredientRow @endif" @if(isset($ingredient)) data-id="{{ $ingredient->ingredient_id }}" @endif>
    <div class="col name">@if(isset($ingredient)){{ $ingredient->ingredient->name }}@endif</div>
    <div class="col">
        <div class="form-group ingredientValue">
            <label>{{ __('Wartość') }}</label>
            <input class="form-control" type="number" min="0.0" step="0.1" @if(isset($ingredient))name="recipe[{{ $id }}][ingredient][{{ $ingredient->ingredient_id }}][value]" value="{{ $ingredient->value }}"@endif/>
        </div>
    </div>
    <div class="col">
        <div class="form-group ingredientMeasure">
            <label @if(isset($ingredient)) for="recipe[{{ $id }}][ingredient][{{ $ingredient->ingredient_id }}][unit]" @endif>{{ __('Miara') }}</label>
            <select class="form-control" @if(isset($ingredient)) name="recipe[{{ $id }}][ingredient][{{ $ingredient->ingredient_id }}][unit]" @endif>
                @foreach(\App\Ingredient::units() as $id => $unit)
                    <option value="{{ $id }}" {{ ((isset($ingredient) && $id == $ingredient->unit) || (!isset($ingredient) && $id == '1') ? 'selected="selected"' : '') }}>{{ $unit['name'] }}({{ \App\Ingredient::getAbbreviation($unit['id']) }})</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <button class="removeIngredient btn btn-danger w-100">{{ __('Usuń składnik') }}</button>
        </div>
    </div>
</div>
