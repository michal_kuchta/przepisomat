<div class="col-3 uploadImageParent @if(isset($photo)) recipeImage @else imageTemplate @endif p-3">
    <div class="row">
        <div class="col">
            <img class='imagePreview pb-3' style="max-width: 100%; max-height: 168px;" @if(isset($photo)) src="{{ $photo->photo->url }}" @endif /><br>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file btn-imageFile">
                            {{ __('Wybierz…') }} <input type="file" class="imageInput">
                        </span>
                    </span>
                    <input type="text" class="form-control imageName" readonly>
                    <input type="hidden" class="form-control imagePath" @if(isset($photo)) name="recipe[{{ $id }}][images][]" value="{{ $photo->photo_id }}" @endif>
                </div>
            </div>
            <span class="btn btn-danger deleteImage w-100">{{ __('Usuń zdjęcie') }}</span>
        </div>
    </div>
</div>
