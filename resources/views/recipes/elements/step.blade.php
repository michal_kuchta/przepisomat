<div class="row uploadImageParent @if(isset($step)) stepRow @else stepTemplate @endif pb-4" data-id="@if(isset($step)){{ $id+1 }}@endif">
    <div class="col">
        <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-2 stepsChanging">
                <table>
                    <tr><td class="align-bottom text-center"><i class="fas fa-arrow-up moveStepUp"></i></td></tr>
                    <tr><td class="align-top text-center"><i class="fas fa-arrow-down moveStepDown"></i></td></tr>
                </table>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10">
                <div class="row">
                    <div class="col stepHeader">
                        <h4>{{ __('Krok') }} <span class="stepId">@if(isset($step)){{ $id+1 }}@endif</span></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col form-group">
                                <label>{{ __('Opis') }}</label>
                                <textarea class="stepDescription form-control w-100" rows="4" style="width: 100%" @if(isset($step)) name="recipe[{{ $recipeId }}][step][{{$id+1}}][description]" @endif>@if(isset($step)){{ $step->description }}@endif</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>{{ __('Zdjęcie') }}</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{ __('Wybierz…') }} <input type="file" class="stepImageInput">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control stepImageName" readonly>
                                        <input type="hidden" class="form-control imagePath" @if(isset($step)) name="recipe[{{ $recipeId }}][step][{{$id+1}}][photo]" value="{{ $step->photo ? $step->photo->id : '' }}" @endif>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="removeStep btn btn-danger w-100">{{ __('Usuń krok') }}</button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <img class='imagePreview' style="max-width: 100%; max-height: 168px;" @if(isset($step) && $step->photo) src="{{ $step->photo->url }}" @endif/><br>
                        <span class="btn btn-danger deletePhoto mt-3 w-100 d-none">{{ __('Usuń zdjęcie') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
