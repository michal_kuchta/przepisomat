@extends('layouts.main')

@section('content')
    <span class="btn btn-success addRecipeButton">{{ __('Podepnij nowy przepis') }}</span>
    <span class="btn btn-info addExistingRecipeButton">{{ __('Podepnij istniejący przepis') }}</span>
    {!! FluentForm::standard()->method('POST')->attr('enctype', 'multipart/form-data') !!}
        @csrf
        <div class="recipes-box">
            @if(!empty($entities))
                @foreach($entities as $key => $entity)
                    @include('recipes.row', ['id' => $key, 'entity' => $entity])
                @endforeach
            @else
                @include('recipes.row', ['id' => 0])
            @endif
        </div>
        <br />

        <span class="btn btn-success addRecipeButton">{{ __('Podepnij nowy przepis') }}</span>
        <span class="btn btn-info addExistingRecipeButton">{{ __('Podepnij istniejący przepis') }}</span>
        <br>
        <br>
        <div class="form-group">
            <input type="submit" class="btn btn-primary pl-3" value="{{ __('Zapisz') }}" />
            <a class="btn btn-danger" href="{{ route('recipes.getList') }}">{{ __('Powrót') }}</a>
        </div>
    {!! FluentForm::close() !!}
    <script type="text/javascript">
        var RecipeForm = {

            debug: true,

            addExistingRecipeButtonSelector: '.addExistingRecipeButton',
            selectExistingRecipeSelector: '.jqAddExistingRecipe',

            addRecipeButtonSelector: '.addRecipeButton',
            deleteRecipeButtonSelector: '.deleteRecipe',

            recipeRowSelector: '.recipe-row',
            recipesBoxSelector: '.recipes-box',
            recipeImageClass: 'recipeImage',
            recipeImageSelector: '.recipeImage',

            deleteRecipeImageButtonSelector: '.deleteImage',

            ingredientsSelector: '.ingredients',
            ingredientSelectSelector: '.jqAddIngredient',
            ingredientRowClass: 'ingredientRow',
            ingredientRowSelector: '.ingredientRow',
            ingredientTemplateClass: 'ingredientTemplate',
            ingredientTemplateSelector: '.ingredientTemplate',
            ingredientValueLabelSelector: '.ingredientValue label',
            ingredientValueInputSelector: '.ingredientValue input',
            ingredientMeasureLabelSelector: '.ingredientMeasure label',
            ingredientMeasureInputSelector: '.ingredientMeasure select',

            deleteIngredientButtonSelector: '.removeIngredient',

            stepIdSelector: '.stepId',
            stepDescriptionSelector: '.stepDescription',
            stepRowClass: 'stepRow',
            stepRowSelector: '.stepRow',
            stepTemplateClass: 'stepTemplate',
            stepTemplateSelector: '.stepTemplate',
            stepsBoxSelector: '.steps',
            stepImageNameSelector: '.stepImageName',
            stepImageInputSelector: '.stepImageInput',
            stepFileInputSelector: '.btn-file :file',
            moveStepUpSelector: '.moveStepUp',
            moveStepDownSelector: '.moveStepDown',

            addStepButtonSelector: '.addNewStep',
            deleteStepButtonSelector: '.removeStep',

            imageFileInputSelector: '.btn-imageFile :file',
            imagesTemplateClass: 'imageTemplate',
            imagesTemplateSelector: '.imageTemplate',
            imagesBoxSelector: '.imagesBox',
            photosBoxSelector: '.photos',
            uploadImageParentSelector: '.uploadImageParent',
            imagePathSelector: '.imagePath',
            imagePreviewSelector: '.imagePreview',
            deletePhotoSelector: '.deletePhoto',

            tabPaneSelector: '.tab-pane',
            submitButtonSelector: 'input[type="submit"]',
            imageInputSelector: '.imageInput',
            addImageButtonSelector: '.addNewImage',

            Init: function()
            {
                $(document).on('click', RecipeForm.addExistingRecipeButtonSelector, function(e){
                    RecipeForm.Log(RecipeForm.addExistingRecipeButtonSelector + ' - clicked');
                    RecipeForm.LoadExistingRecipeTemplate();
                });

                $(document).on("select2:selecting", RecipeForm.selectExistingRecipeSelector, function(e) {
                    RecipeForm.Log(RecipeForm.selectExistingRecipeSelector + ' - selected');
                    RecipeForm.LoadRecipe($(this).closest(RecipeForm.recipeRowSelector), $(this).closest(RecipeForm.recipeRowSelector).data('id'), e.params.args.data.id);
                });

                $(document).on('click', RecipeForm.addRecipeButtonSelector, function(e){
                    RecipeForm.Log(RecipeForm.addRecipeButtonSelector + ' - clicked');
                    RecipeForm.LoadRecipeTemplate();
                });

                $(document).on('click', RecipeForm.deleteRecipeButtonSelector, function () {
                    RecipeForm.Log(RecipeForm.deleteRecipeButtonSelector + ' - clicked');

                    $(this).closest(RecipeForm.recipeRowSelector).find('textarea[data-editor]').each(function ()
                    {
                        tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'recipe.'+$(this).closest(RecipeForm.recipeRowSelector).data('id')+'.description');
                        tinymce.EditorManager.execCommand('mceRemoveEditor',true, $(this).closest(RecipeForm.recipeRowSelector).data('id'));
                    });
                    $(this).closest(RecipeForm.recipeRowSelector).remove();
                });

                $(RecipeForm.ingredientSelectSelector).each(function(key, el){
                    RecipeForm.InitializeIngredient(el);
                });

                $(document).on("select2:selecting", RecipeForm.ingredientSelectSelector, function(e) {
                    RecipeForm.Log(RecipeForm.ingredientSelectSelector + ' - selected');
                    RecipeForm.LoadIngredientTemplate($(this), e.params.args.data);
                });

                $(document).on("click", RecipeForm.deleteIngredientButtonSelector, function(e){
                    e.preventDefault();
                    RecipeForm.Log(RecipeForm.deleteIngredientButtonSelector + ' - clicked');
                    $(this).closest(RecipeForm.ingredientRowSelector).remove();
                });

                $(document).on('click', RecipeForm.addStepButtonSelector, function(e){
                    RecipeForm.Log(RecipeForm.addStepButtonSelector + ' - clicked');
                    RecipeForm.LoadNewStepTemplate($(this));
                });

                $(document).on("click", RecipeForm.deleteStepButtonSelector, function(e){
                    e.preventDefault();
                    RecipeForm.Log(RecipeForm.deleteStepButtonSelector + ' - clicked');
                    RecipeForm.RemoveStep($(this));
                });

                $(document).on('change', RecipeForm.stepFileInputSelector, function() {
                    RecipeForm.Log(RecipeForm.stepFileInputSelector + ' - changed');
                    RecipeForm.ClearFileName($(this));
                });
                $(document).on('change', RecipeForm.imageFileInputSelector, function() {
                    RecipeForm.Log(RecipeForm.imageFileInputSelector + ' - changed');
                    RecipeForm.ClearFileName($(this));
                });

                $(document).on('fileselect', RecipeForm.stepFileInputSelector, function(event, label) {
                    RecipeForm.Log(RecipeForm.stepFileInputSelector + ' - file selected');
                    RecipeForm.HandleFileSelection($(this), label);
                });
                $(document).on('fileselect', RecipeForm.imageFileInputSelector, function(event, label) {
                    RecipeForm.Log(RecipeForm.imageFileInputSelector + ' - file selected');
                    RecipeForm.HandleFileSelection($(this), label);
                });

                $(document).on('change', RecipeForm.stepImageInputSelector, function(){
                    RecipeForm.Log(RecipeForm.stepImageInputSelector + ' - changed');
                    RecipeForm.UploadFile($(this));
                });

                $(document).on('click', RecipeForm.moveStepUpSelector, function(){
                    RecipeForm.Log(RecipeForm.moveStepUpSelector + ' - clicked');
                    console.log($(this).closest(RecipeForm.stepRowSelector).data('id'));
                    RecipeForm.MoveStep($(this).closest(RecipeForm.stepRowSelector), 'up');
                });
                $(document).on('click', RecipeForm.moveStepDownSelector, function(){
                    RecipeForm.Log(RecipeForm.moveStepDownSelector + ' - clicked');
                    console.log($(this).closest(RecipeForm.stepRowSelector).data('id'));
                    RecipeForm.MoveStep($(this).closest(RecipeForm.stepRowSelector), 'down');
                });

                $(document).on('click', RecipeForm.deletePhotoSelector, function(e){
                    RecipeForm.Log(RecipeForm.deletePhotoSelector + ' - clicked');
                    e.preventDefault();
                    RecipeForm.DeleteStepPhoto($(this));
                });

                $(document).on('change', RecipeForm.imageInputSelector, function(){
                    RecipeForm.Log(RecipeForm.imageInputSelector + ' - changed');
                    RecipeForm.UploadFile($(this));
                });

                $(document).on('click', RecipeForm.deleteRecipeImageButtonSelector, function(e){
                    RecipeForm.Log(RecipeForm.deleteRecipeImageButtonSelector + ' - clicked');
                    e.preventDefault();
                    RecipeForm.DeleteRecipePhoto($(this));
                });

                $(document).on('click', RecipeForm.addImageButtonSelector, function (e) {
                    RecipeForm.Log(RecipeForm.addImageButtonSelector + ' - clicked');
                    e.preventDefault();
                    RecipeForm.LoadNewImageTemplate($(this));
                });
            },

            Log: function(message)
            {
                if (RecipeForm.debug == true)
                {
                    console.log(message);
                }
            },

            LoadRecipeTemplate: function()
            {
                RecipeForm.Log('Loading recipe template');
                $.post("{{ route('recipes.getRecipeTemplate') }}", {
                    id: $(RecipeForm.recipeRowSelector).length
                }, function(data){
                    var form = $(data);
                    $(RecipeForm.recipesBoxSelector).append(form.find(RecipeForm.recipesBoxSelector).html());
                    RecipeForm.InitializeRow();
                });
            },

            LoadRecipe: function(el, row_id, recipe_id)
            {
                RecipeForm.Log('Loading recipe');
                $.post("{{ route('recipes.getRecipeTemplate') }}", {
                    id: row_id,
                    recipe_id: recipe_id
                }, function(data){
                    var form = $(data);
                    $(el).replaceWith(form.find(RecipeForm.recipesBoxSelector).html());
                    RecipeForm.InitializeRow($('.recipe-row-'+row_id));
                });
            },

            LoadExistingRecipeTemplate: function()
            {
                RecipeForm.Log('Loading existing recipe template');
                $.post("{{ route('recipes.getExistingRecipeTemplate') }}", {
                    id: $(RecipeForm.recipeRowSelector).length
                }, function(data){
                    $(RecipeForm.recipesBoxSelector).append(data);
                    RecipeForm.InitializeExistingRow($(RecipeForm.recipesBoxSelector).find(RecipeForm.recipeRowSelector).last().find(RecipeForm.selectExistingRecipeSelector));
                });
            },

            InitializeIngredient: function (el)
            {
                RecipeForm.Log('Initializing ingredient');
                $(el).select2({
                    //Wykonanie ajaxa po wpisaniu czegoś w inpucie
                    ajax: {
                        url: "{{ route('ingredients.getListJson') }}",
                        type: "GET",
                        //Przetworzenie wprowadzonych danych oraz dodanie ich do linku
                        data: function (params) {
                            var query = {
                                search: params.term,
                                selected: RecipeForm.GetSelectedIngredients(el),
                                page: params.page || 1
                            }
                            return query;
                        },
                        dataType: "json",

                        //Prztetowrzenie zwróconych danych
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.results,
                                pagination: data.pagination
                            };
                        }
                    },
                    dropdownParent: $(el).parent(),
                    templateSelection: function(state){
                        return "";
                    }
                });
            },

            InitializeRow: function(el)
            {
                if(typeof el === "undefined") {
                    el = $(RecipeForm.recipesBoxSelector).find(RecipeForm.recipeRowSelector).last();
                }

                RecipeForm.Log('Initializing row');
                RecipeForm.InitializeIngredient($(el).find('.jqAddIngredient'));
                $(el).find('textarea[data-editor]').each(function ()
                {
                    tinymce.init($(this).data('config'));
                })
            },

            InitializeExistingRow: function (el)
            {
                RecipeForm.Log('Initializing existing row');
                $(el).select2({
                    //Wykonanie ajaxa po wpisaniu czegoś w inpucie
                    ajax: {
                        url: "{{ route('recipes.getListJson') }}",
                        type: "GET",
                        //Przetworzenie wprowadzonych danych oraz dodanie ich do linku
                        data: function (params) {
                            var query = {
                                search: params.term,
                                page: params.page || 1
                            }
                            return query;
                        },
                        dataType: "json",

                        //Prztetowrzenie zwróconych danych
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.results,
                                pagination: data.pagination
                            };
                        }
                    },
                    dropdownParent: $(el).parent(),
                    templateSelection: function(state){
                        return "";
                    }
                });
            },

            GetSelectedIngredients: function(el)
            {
                RecipeForm.Log('Getting selected ingredients');
                var ingredients = '';
                $(el).closest(RecipeForm.tabPaneSelector).find(RecipeForm.ingredientRowSelector).each(function(id, el){
                    if(ingredients.length > 0)
                    {
                        ingredients = ingredients + ',';
                    }
                    ingredients = ingredients + $(el).attr('data-id');
                });
                return ingredients;
            },

            LoadIngredientTemplate: function(el, params)
            {
                if (params.id == 0)
                {
                    $.post("{{ route('api.ingredients.postAdd') }}", {
                        name: params.text
                    }, function(response){
                        RecipeForm.CreateIngredientTemplate($(el), response);
                    });
                }
                else
                {
                    RecipeForm.CreateIngredientTemplate($(el), params);
                }
            },

            CreateIngredientTemplate: function(el, data)
            {
                RecipeForm.Log('Creating ingredient template');
                var clone = $(el).closest(RecipeForm.tabPaneSelector).find(RecipeForm.ingredientTemplateSelector).clone();
                clone.removeAttr('id');
                clone.removeClass(RecipeForm.ingredientTemplateClass);
                clone.addClass(RecipeForm.ingredientRowClass);
                clone.attr('data-id', data.id);
                clone.find('div.col.name').html(data.text);
                clone.find(RecipeForm.ingredientValueLabelSelector).attr('for', 'recipe['+$(el).attr('recipe-id')+'][ingredient]['+data.id+'][value]');
                clone.find(RecipeForm.ingredientValueInputSelector).attr('name', 'recipe['+$(el).attr('recipe-id')+'][ingredient]['+data.id+'][value]');
                clone.find(RecipeForm.ingredientMeasureLabelSelector).attr('for', 'recipe['+$(el).attr('recipe-id')+'][ingredient]['+data.id+'][unit]');
                clone.find(RecipeForm.ingredientMeasureInputSelector).attr('name', 'recipe['+$(el).attr('recipe-id')+'][ingredient]['+data.id+'][unit]');
                clone.insertBefore($(el).closest(RecipeForm.ingredientsSelector).find('hr').first());
            },

            SetStepNames: function(clone, id, recipeId)
            {
                RecipeForm.Log('setting step names');
                clone.find(RecipeForm.stepIdSelector).html(id);
                clone.attr('data-id', id);
                clone.attr('data-recipe-id', recipeId);
                clone.find(RecipeForm.stepDescriptionSelector).attr('name', 'recipe['+recipeId+'][step]['+id+'][description]');
                clone.find(RecipeForm.imagePathSelector).attr('name', 'recipe['+recipeId+'][step]['+id+'][photo]');
                return clone;
            },

            LoadNewStepTemplate: function(el)
            {
                RecipeForm.Log('Loading step template');
                var counter = parseInt($(el).closest(RecipeForm.recipeRowSelector).find(RecipeForm.stepRowSelector).length);
                var clone = RecipeForm.SetStepNames($(el).closest(RecipeForm.tabPaneSelector).find(RecipeForm.stepTemplateSelector).clone(), counter+1, $(el).data('id'));
                clone.removeAttr('id');
                clone.removeClass(RecipeForm.stepTemplateClass);
                clone.addClass(RecipeForm.stepRowClass);
                clone.insertBefore($(el).closest(RecipeForm.stepsBoxSelector).find('hr').first());
            },

            RemoveStep: function(el)
            {
                RecipeForm.Log('Removing step');
                var recipe_id = $(el).closest(RecipeForm.stepRowSelector).attr('data-recipe-id');
                $(el).closest(RecipeForm.stepRowSelector).remove();
                RecipeForm.ReorderStepIds(recipe_id);
            },

            ReorderStepIds: function(recipe_id)
            {
                RecipeForm.Log('ReorderingStepsIds');
                var counter = 1;
                $(RecipeForm.stepRowSelector).each(function(index, el){
                    RecipeForm.SetStepNames($(el), counter, recipe_id);
                    counter = counter + 1;
                });
            },

            MoveStep: function(row, direction){
                var id = parseInt($(row).data('id'));
                var recipe_id = parseInt($(row).data('recipe-id'));

                var clone = $(row).clone();

                if (direction == 'up')
                {
                    var previous_id = id - 1;
                    $(clone).insertBefore($(row).closest(RecipeForm.stepsBoxSelector).find(RecipeForm.stepRowSelector+'[data-id="'+previous_id+'"]').first());
                }
                else if(direction == 'down')
                {
                    var next_id = id + 1;
                    $(clone).insertAfter($(row).closest(RecipeForm.stepsBoxSelector).find(RecipeForm.stepRowSelector+'[data-id="'+next_id+'"]').first());
                }

                $(row).remove();

                RecipeForm.ReorderStepIds(recipe_id);
            },

            ClearFileName: function(el)
            {
                RecipeForm.Log('Clearing file name');
                var input = $(el);
                var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            },

            HandleFileSelection: function(el, label)
            {
                RecipeForm.Log('Handling file selection');
                var input = $(el).parents('.input-group').find(':text');
                var log = label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            },

            ToggleSubmitButton: function(bool)
            {
                RecipeForm.Log('Toggling submit button');
                $(RecipeForm.submitButtonSelector).attr("disabled", bool);
            },

            UploadFile: function(el)
            {
                var input = el[0];
                RecipeForm.Log('uploading file');
                console.log(input);
                console.log(el);
                if (input.files && input.files[0]) {
                    var fd = new FormData();
                    var files = input.files[0];
                    fd.append('file', files);
                    RecipeForm.ToggleSubmitButton(true);

                    $.ajax({
                        url: "{{ route('photo.store') }}",
                        type: "post",
                        data: fd,
                        contentType: false,
                        processData: false,
                        success: function(data){
                            $(input).closest(RecipeForm.uploadImageParentSelector).find(RecipeForm.imagePreviewSelector).attr('src', data.path);
                            $(input).closest(RecipeForm.uploadImageParentSelector).find(RecipeForm.imagePathSelector).attr('value', data.id);
                            RecipeForm.ToggleSubmitButton(false);
                        },
                        error:function(){
                            RecipeForm.ToggleSubmitButton(false);
                        }
                    });

                }
            },

            DeleteStepPhoto: function(el)
            {
                RecipeForm.Log('Deleting step photo');
                $(this).closest(RecipeForm.stepRowSelector).find(RecipeForm.imagePreviewSelector).removeAttr('src');
                $(this).closest(RecipeForm.stepRowSelector).find(RecipeForm.deletePhotoSelector).addClass('d-none');
                $(this).closest(RecipeForm.stepRowSelector).find(RecipeForm.stepImageNameSelector).val('');
                $(this).closest(RecipeForm.stepRowSelector).find(RecipeForm.stepImageInputSelector).val('');
                $(this).closest(RecipeForm.stepRowSelector).find(RecipeForm.imagePathSelector).val('');
            },

            DeleteRecipePhoto: function(el)
            {
                RecipeForm.Log('Deleting recipe photo');
                $(el).closest(RecipeForm.recipeImageSelector).remove();
            },

            LoadNewImageTemplate: function(el)
            {
                RecipeForm.Log('Loading image template');
                var clone = $(el).closest(RecipeForm.tabPaneSelector).find(RecipeForm.imagesTemplateSelector).clone();
                clone.removeAttr('id');
                clone.removeClass(RecipeForm.imagesTemplateClass);
                clone.addClass(RecipeForm.recipeImageClass);
                clone.find(RecipeForm.imagePathSelector).attr('name', 'recipe['+$(el).data('id')+'][images][]');
                $(el).closest(RecipeForm.photosBoxSelector).find(RecipeForm.imagesBoxSelector).append(clone);
            }
        };

        $(document).ready(function()
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            RecipeForm.Init();

            $(document).on('change', '.share_type', function(){
                var value = $(this).val();
                var id = $(this).attr('data-id');

                if (value != '' && value != undefined && id != '' && id != undefined)
                {
                    if ($(this).val() == {{ \App\SharedRecipe::TYPE_RECIPE_CUSTOM }})
                        $('#shareEmailsBox-'+$(this).attr('data-id')).show();
                    else
                        $('#shareEmailsBox-'+$(this).attr('data-id')).hide();
                }
            });
        });
    </script>
@endsection
