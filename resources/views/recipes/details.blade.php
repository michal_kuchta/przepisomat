@extends('layouts.main')

@section('content')
        <div class="row">
            <div class="col-lg-6 text-left">
                @if($entity->category)
                    <h4 class="header mt-3" style="font-size:17px">{!! __('Kategoria:') !!} <a href="{{ $entity->category->getUrl() }}">{!! $entity->category->name !!}</a></h4>
                @endif
            </div>
            @if($entity->user_id === auth()->id())
                <div class="col-lg-6 text-right">
                    <a href="{{ route('recipes.getEdit', [$entity->id]) }}" class="btn btn-warning">{{ __('Edytuj') }}</a>
                    <a href="{{ route('recipes.getDelete', [$entity->id]) }}" class="btn btn-danger">{{ __('Usuń') }}</a>
                </div>
            @endif
        </div>
    <div class="row">
        <div class="col-lg-6">
            <div id="carousel_recipe" class="carousel slide ml-auto mr-auto" data-ride="carousel">
                @if (count($entity->photos) > 1)
                    <ol class="carousel-indicators">
                        @foreach($entity->photos as $id => $photo)
                            <li data-target="#carousel_recipe" data-slide-to="{{ $id }}" class="{{ $id == 0 ? 'active' : '' }}"></li>
                        @endforeach
                    </ol>
                @endif
                <div class="carousel-inner">
                    @if(count($entity->photos) > 0)
                        @foreach($entity->photos as $id => $photo)
                            <div class="carousel-item {{ $id == 0 ? 'active' : '' }}">
                                <img class="d-block ml-auto mr-auto" style="width: 100%;" src="{{ $photo->photo->url }}">
                            </div>
                        @endforeach
                    @else
                        <div class="carousel-item active">
                            <img class="d-block ml-auto mr-auto" style="width: 100%;" src="{{ $entity->getPlaceholder() }}">
                        </div>
                    @endif
                </div>
                @if (count($entity->photos) > 1)
                    <a class="carousel-control-prev" href="#carousel_recipe" role="button" data-slide="prev">
                        <span class="left-arrow" aria-hidden="true"></span>
                        <span class="sr-only">{{ __('Poprzednie') }}</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel_recipe" role="button" data-slide="next">
                        <span class="right-arrow" aria-hidden="true"></span>
                        <span class="sr-only">{{ __('Następne') }}</span>
                    </a>
                @endif
            </div>
        </div>

        <div class="col-lg-6">
            <h3>{{ $entity->name }}</h3>
            <hr />
            <h4 class="header mt-3" style="font-size:17px">{!! __('Składniki') !!}</h4>
            @foreach($entity->ingredients as $id => $ingredient)
                <div class="row">
                    <div class="col-1">{!! FluentForm::checkbox('ingredient.'.$id) !!}</div>
                    <div class="col-1">
                        <span
                            class="add-to-cart-btn"
                            data-ingredient="{{ $ingredient->ingredient_id }}"
                            data-unit="{{ $ingredient->getBaseUnit()['id'] }}"
                            data-value="{{ $ingredient->getValue(true) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="{{ __('Dodaj do listy zakupów') }}"
                        >
                            <i class="fas fa-shopping-cart"></i>
                        </span>
                    </div>
                    <div class="col-4">{{ $ingredient->ingredient->name }}</div>
                    <div class="col-6">{{ $ingredient->getValue(true) }} {{ $ingredient->getBaseAbbreviation() }}</div>
                </div>
                @if(!$loop->last)
                    <div class="row">
                        <div class="col-8"><div style="border-top: 1px solid rgba(168,168,168,0.54)"></div></div>
                        <div class="col-4"></div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-12 text-center">
            <div class="row additional-data-row">
                <div class="col-lg-4 rating">
                    <div class="star star-1" data-id="1"><i class="{{ $entity->getRatingValue() >= 1 ? 'fas' : 'far' }} fa-star"></i></div>
                    <div class="star star-2" data-id="2"><i class="{{ $entity->getRatingValue() >= 2 ? 'fas' : 'far' }} fa-star"></i></div>
                    <div class="star star-3" data-id="3"><i class="{{ $entity->getRatingValue() >= 3 ? 'fas' : 'far' }} fa-star"></i></div>
                    <div class="star star-4" data-id="4"><i class="{{ $entity->getRatingValue() >= 4 ? 'fas' : 'far' }} fa-star"></i></div>
                    <div class="star star-5" data-id="5"><i class="{{ $entity->getRatingValue() >= 5 ? 'fas' : 'far' }} fa-star"></i></div>
                    <div class="star-count">{!! $entity->rating_count !!}</div>
                </div>
                <div class="col-lg-2 difficulty">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="bar-minimum{{ $entity->difficulty == 1 ? ' active' : '' }}"></div>
                            <div class="bar-medium{{ $entity->difficulty == 2 ? ' active' : '' }}"></div>
                            <div class="bar-maximum{{ $entity->difficulty == 3 ? ' active' : '' }}"></div>
                        </div>
                    </div>
                    {{ $entity->getDifficulty() }}
                </div>
                <div class="col-lg-2 preparing-time">
                    <i class="far fa-clock"></i><br>
                    {{ $entity->preparing_time }} {{ __('min.') }}
                </div>
                <div class="col-lg-2 text-center portions">
                    <i class="far fa-user"></i><br>
                    {{ $entity->portions }} {{ __('os.') }}
                </div>
                @if ($entity->estimated_price > 0)
                    <div class="col-lg-2 text-center portions">
                        <i class="fas fa-dollar-sign"></i><br>
                        {{ $entity->estimated_price }} {{ __('zł') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if(!empty($entity->description))
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="header">{{ __('Opis') }}</h1>
                {!! $entity->description !!}
            </div>
        </div>
    @endif

    @if(count($entity->subRecipes) > 0 && !isset($secondLevel))
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="header">{{ __('Przepisy połączone') }}</h1>
                @foreach($entity->subRecipes as $recipe)
                    <a href="{{ route('recipes.getDetails', [$recipe->id]) }}">{{ $recipe->name }}</a>
                @endforeach
            </div>
        </div>
    @endif

    @if(!empty($entity->steps))
        <hr>
        <div class="row">
            <div class="col-lg-12">
                @foreach($entity->steps as $step)
                    <h3 class="header mt-3">{{ __('Krok: ') }}{{ $step->position }}</h3>
                    <div class="row">
                        <div class="col-lg-4"><img src="{{ $step->photo ? $step->photo->url : $entity->getPlaceholder() }}" style="max-width: 100%; max-height: 500px"></div>
                        <div class="col-lg-8">{{ $step->description }}</div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        var Rating = {
            Init: function(){
                $('.star').hover(function(){
                    var id = parseInt($(this).attr('data-id'));

                    Rating.SelectStar(id);
                }, function(){
                    Rating.DeselectStar();
                });
                $('.star').on('click', function(){
                    Rating.SaveRating(parseInt($(this).attr('data-id')));
                });
            },

            SelectStar: function(id){
                $('.rating .star').each(function(key, el){
                    var elId = parseInt($(el).attr('data-id'));

                    if ($(el).find('i').hasClass('fas'))
                        $(el).find('i').attr('data-selected', '1');

                    if (id >= elId)
                        $(el).find('i').removeClass('far').addClass('fas');
                    else
                        $(el).find('i').removeClass('fas').addClass('far');
                });
            },

            DeselectStar: function(){
                $('.rating .star').each(function(key, el){
                    if ($(el).find('i').attr('data-selected') != 1)
                        $(el).find('i').removeClass('fas').addClass('far');
                });
            },

            SaveRating: function(id){
                $.post('{{ route('recipes.postRate', [$entity->id]) }}', {
                    rating: id
                }, function(data){
                    if (data.success)
                    {
                        $('.rating .star').each(function(key, el){
                            var elId = parseInt($(el).attr('data-id'));

                            if (data.rate >= elId)
                                $(el).find('i').removeClass('far').addClass('fas');
                            else
                                $(el).find('i').removeClass('fas').addClass('far');
                        });
                        $('.rating .star-count').html(data.count);
                    }
                    else
                    {
                        alert(data.message);
                    }
                });
            }
        };

        $(document).ready(function () {
            Rating.Init();
        });
    </script>
    @include('scripts/shopping_list')
@endsection