@extends('layouts.main')

@section('content')
    <h3 class="header">{!! __('Lista zakupów') !!}</h3>
    @foreach($rows as $row)
        <div class="row">
            <div class="col-1"><span class="add-to-cart-btn remove-row" data-ingredient="{{ $row['ingredient_id'] }}" data-toggle="tooltip" data-placement="top" title="{{ __('Usuń z listy zakupów') }}"><i class="fas fa-shopping-cart"></i></span></div>
            <div class="col-4">{{ $row['name'] }}</div>
            <div class="col-7">{{ $row['total'] }} {{ $row['abbreviation'] }}</div>
        </div>
        @if(!$loop->last)
            <div class="row">
                <div class="col-8"><div style="border-top: 1px solid rgba(168,168,168,0.54)"></div></div>
                <div class="col-4"></div>
            </div>
        @endif
    @endforeach
@endsection

@section('scripts')
    @include('scripts/shopping_list')
@endsection