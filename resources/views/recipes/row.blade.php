<div class="card mt-3 tab-card recipe-row recipe-row-{{ $id }}" data-id="{{ $id }}">
    <div class="card-header tab-card-header">
        <ul class="nav nav-tabs card-header-tabs" id="myTab{{ $id }}" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="general-tab-{{ $id }}" data-toggle="tab" href="#general-{{ $id }}" role="tab" aria-controls="{{ __('Dane podstawowe') }}" aria-selected="true">{{ __('Dane podstawowe') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="ingredients-tab-{{ $id }}" data-toggle="tab" href="#ingredients-{{ $id }}" role="tab" aria-controls="{{ __('Składniki') }}" aria-selected="false">{{ __('Składniki') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="steps-tab-{{ $id }}" data-toggle="tab" href="#steps-{{ $id }}" role="tab" aria-controls="{{ __('Kroki') }}" aria-selected="false">{{ __('Kroki') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="photos-tab-{{ $id }}" data-toggle="tab" href="#photos-{{ $id }}" role="tab" aria-controls="{{ __('Zdjęcia') }}" aria-selected="false">{{ __('Zdjęcia') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="sharing-tab-{{ $id }}" data-toggle="tab" href="#sharing-{{ $id }}" role="tab" aria-controls="{{ __('Udostępnianie') }}" aria-selected="false">{{ __('Udostępnianie') }}</a>
            </li>
        </ul>
        @if((int)$id > 0)
            <span class="btn btn-danger deleteRecipe">X</span>
        @endif
    </div>

    @if(isset($entity) && !empty($entity->id))
        {!! FluentForm::hidden('recipe['. $id .'][id]', $entity->id) !!}
    @endif

    <div class="tab-content" id="myTab{{ $id }}Content">
        <div class="tab-pane fade show active p-3" id="general-{{ $id }}" role="tabpanel" aria-labelledby="general-tab-{{ $id }}">
            <div class="row">
                <div class="col-lg-7 col-md-12">
                    {!! FluentForm::group()->text('recipe['.$id.'][name]', isset($entity) ? $entity->name : '')->required(true)->label(__('Nazwa')) !!}
                    {!! FluentForm::group()->editor('recipe['.$id.'][description]', isset($entity) ? $entity->description : '')->label(__('Opis'))->rows(5) !!}
                </div>
                <div class="col-lg-5 col-md-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            {!! FluentForm::group()->number('recipe['.$id.'][portions]', isset($entity) && !empty($entity->portions) ? $entity->portions : null)->placeholder(1)->label(__('Ilość porcji'))->min(1) !!}
                        </div>
                        <div class="col-lg-6 col-md-6">
                            {!! FluentForm::group()->number('recipe['.$id.'][preparing_time]', isset($entity) && !empty($entity->preparing_time) ? $entity->preparing_time : null)->placeholder(0)->label(__('Czas przygotowania (minuty)'))->min(0) !!}
                        </div>
                    </div>
                    {!! FluentForm::group()->number('recipe['.$id.'][estimated_price]', isset($entity) && !empty($entity->estimated_price) ? $entity->estimated_price : null)->placeholder(0)->label(__('Szacunkowy koszt (zł)'))->min(0)->step(0.1) !!}
                    {!! FluentForm::group()->select('recipe['.$id.'][difficulty]', \App\Recipe::getDifficulties(), isset($entity) && !empty($entity->difficulty) ? $entity->difficulty : 1)->label(__('Trudność')) !!}
                    {!! FluentForm::group()->select('recipe['.$id.'][category_id]', \App\Category::getOptions(), isset($entity) && !empty($entity->category_id) ? $entity->category_id : null)->label(__('Kategoria'))->placeholder(__('Brak kategorii')) !!}
                </div>
            </div>
        </div>

        <div class="tab-pane p-3 ingredients" id="ingredients-{{ $id }}" role="tabpanel" aria-labelledby="ingredients-tab-{{ $id }}">
            @include('recipes.elements.ingredient', ['id' => $id])

            @if(isset($entity) && $entity->ingredients && $entity->ingredients->count() > 0)
                @foreach($entity->ingredients as $ingredient)
                    @include('recipes.elements.ingredient', ['ingredient' => $ingredient, 'id' => $id])
                @endforeach
            @endif
            <hr />
            <div class="form-group">
                <label for="nazwa">{{ __('Dodaj składnik') }}</label>
                <select class="form-control w-50 d-inline jqAddIngredient" style="width: 50%" data-selected="" recipe-id="{{ $id }}"></select>
            </div>
        </div>

        <div class="tab-pane p-3 steps" id="steps-{{ $id }}" role="tabpanel" aria-labelledby="steps-tab-{{ $id }}">
            @include('recipes.elements.step', ['recipeId' => $id])

            @if(isset($entity) && $entity->steps && $entity->steps->count() > 0)
                @foreach($entity->steps as $rowId => $step)
                    @include('recipes.elements.step', ['step' => $step, 'id' => $rowId, 'recipeId' => $id])
                @endforeach
            @endif
            <hr />
            <span class="form-group align-content-end">
                <span class="btn btn-primary addNewStep" data-id="{{ $id }}">{{ __('Dodaj nowy krok') }}</span>
            </span>
        </div>

        <div class="tab-pane p-3 photos" id="photos-{{ $id }}" role="tabpanel" aria-labelledby="photos-tab-{{ $id }}">
            <div id="imagesBox-{{ $id }}" class="imagesBox row mt-3">
                @include('recipes.elements.photo', ['id' => $id])

                @if(isset($entity) && $entity->photos && $entity->photos->count() > 0)
                    @foreach($entity->photos as $photo)
                        @include('recipes.elements.photo', ['photo' => $photo, 'id' => $id])
                    @endforeach
                @endif
            </div>
            <hr />
            <div class="form-group align-content-end">
                <span class="btn btn-primary addNewImage" data-id="{{ $id }}">{{ __('Dodaj zdjęcie') }}</span>
            </div>
        </div>

        <div class="tab-pane p-3 sharing" id="sharing-{{ $id }}" role="tabpanel" aria-labelledby="sharing-tab-{{ $id }}">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="share_type[{{ $id }}]" class="control-label">{!! __('Zasady udostępniania przepisów') !!}</label>
                        <ul data-id="0" class="list-unstyled radio">
                            <li>
                                <label class="share_type">
                                    <input data-id="{{ $id }}" class="share_type" type="radio" name="recipe[{{ $id }}][share_type]" value="{{ \App\SharedRecipe::TYPE_NONE }}" {{ $entity->share_type == \App\SharedRecipe::TYPE_NONE ? 'checked="checked"' : '' }}>
                                    <span></span>
                                    {!! __('Nie udostępniaj') !!}
                                </label>
                            </li>
                            <li>
                                <label class="share_type">
                                    <input data-id="{{ $id }}" class="share_type" type="radio" name="recipe[{{ $id }}][share_type]" value="{{ \App\SharedRecipe::TYPE_RECIPE_CUSTOM }}" {{ $entity->share_type == \App\SharedRecipe::TYPE_RECIPE_CUSTOM ? 'checked="checked"' : '' }}>
                                    <span></span>
                                    {!! __('Udostępnij wybranym') !!}
                                </label>
                            </li>
                            <li>
                                <label class="share_type">
                                    <input data-id="{{ $id }}" class="share_type" type="radio" name="recipe[{{ $id }}][share_type]" value="{{ \App\SharedRecipe::TYPE_RECIPE_ALL }}" {{ $entity->share_type == \App\SharedRecipe::TYPE_RECIPE_ALL ? 'checked="checked"' : '' }}>
                                    <span></span>
                                    {!! __('Udostępnij wszystkim') !!}
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div id="shareEmailsBox-{{ $id }}" {!! $entity->share_type != \App\SharedRecipe::TYPE_RECIPE_CUSTOM ? 'style="display:none"' : '' !!}>
                        <multi-row-field-component
                            name="recipe[{{ $id }}][shared_emails]"
                            :rows="{{ json_encode($entity->sharedEmails() ?? []) }}"
                            placeholder="{!! __('Adres e-mail') !!}"
                            label="{!! __('Udostępnij wybranym') !!}"
                        ></multi-row-field-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
