@extends('layouts.main')

@section('topPanel')
    @include('partials.categories.panel')
@endsection

@section('content')
    {!! FluentForm::standard()->method('POST') !!}
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 text-left">
                <a href="{{ route('recipes.getAdd') }}" class="btn btn-success">{{ __('Dodaj nowy') }}</a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-6">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        {!! FluentForm::select('sort', ['date_desc' => __('Od najnowszych'), 'date_asc' => __('Od najstarszych'),'name_asc' => __('Nazwa: A-Z'), 'name_desc' => __('Nazwa: Z-A')], $sort)->css('d-inline') !!}
                    </div>
                </div>
            </div>
        </div>
    {!! FluentForm::close() !!}

    <div class="d-flex flex-wrap">
        @if($rows && count($rows) > 0)
            @foreach($rows as $id => $row)
                <div class="card card-body p-2 d-flex flex-column m-2 recipe-box" style="max-width: 250px;">
                    <i class="favourite @if (isset($deleteFromList)) deleteFromList @endif fas fa-heart @if ($row->isFavourite()) filled @endif" data-url="{{ route('recipes.postSetFavourite', [$row->id]) }}" title="{!! __('Dodaj do ulubionych') !!}" data-toggle="tooltip" data-placement="top"></i>
                    <div><a href="{{ route('recipes.getDetails', [$row->id]) }}"><img style="width: 100%;" src="{{ $row->getMainPhoto() ?? '' }}"></a></div>
                    <div>
                        <a href="{{ route('recipes.getDetails', [$row->id]) }}" class="page-link" style="color: black; decoration: none">
                            <div class="row">
                                <div class="col-lg-12">
                                    <span class="d-block">{{ $row->name }}</span>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row additional-data-row">
                                        <div class="col-3 preparing-time">
                                            <i class="far fa-star"></i><br>
                                            {{ $row->getRatingValue() }}
                                        </div>
                                        <div class="col-3 difficulty">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="bar-minimum{{ $row->difficulty == 1 ? ' active' : '' }}"></div>
                                                    <div class="bar-medium{{ $row->difficulty == 2 ? ' active' : '' }}"></div>
                                                    <div class="bar-maximum{{ $row->difficulty == 3 ? ' active' : '' }}"></div>
                                                </div>
                                            </div>
                                            {{ $row->getDifficulty() }}
                                        </div>
                                        <div class="col-3 preparing-time">
                                            <i class="far fa-clock"></i><br>
                                            {{ $row->preparing_time }} {{ __('min.') }}
                                        </div>
                                        <div class="col-3 text-center portions">
                                            <i class="far fa-user"></i><br>
                                            {{ $row->portions }} {{ __('os.') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        @else
            <h3>{{ __('Brak przepisów') }}</h3>
        @endif
    </div>

    {!! $pager !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('select[name="sort"]').on('change', function(){
                $(this).closest('form').submit();
            });
        });
    </script>
@endsection