@if($pager->getTotalResults() > 0)
    <div class="row pagination justify-content-center mt-2">
        @if($pager->getCurrentPage() - 1 > 0)
            <span class="page-item">
                    <a class="page-link" href="{!! $pager->getUrl() !!} ">{!! __('pierwsza') !!}</a>
                </span>
            <span class="page-item">
                    <a class="page-link" href="{!! $pager->getUrl() !!}/{!! $pager->getCurrentPage() - 1 !!}">{!! __('poprzednia') !!}</a>
                </span>
        @endif

        @for($i = 5; $i >= 1; $i--)
            @if($pager->getCurrentPage() - $i > 0)
                <span class="page-item">
                        <a class="page-link" href="{!! $pager->getUrl() !!}/{!! $pager->getCurrentPage() - $i !!}">{!! $pager->getCurrentPage() - $i !!}</a>
                    </span>
            @endif
        @endfor

        <span class="page-item active">
                <a class="page-link" href="#">{!! $pager->getCurrentPage() !!}</a>
            </span>

        @for($i = 1; $i < 5; $i++)
            @if($pager->getCurrentPage() + $i <= $pager->getTotalPages())
                <span class="page-item">
                        <a class="page-link" href="{!! $pager->getUrl() !!}/{!! $pager->getCurrentPage() + $i !!}">{!! $pager->getCurrentPage() + $i !!}</a>
                    </span>
            @endif
        @endfor

        @if($pager->getCurrentPage() + 1 <= $pager->getTotalPages())
            <span class="page-item">
                    <a class="page-link" href="{!! $pager->getUrl() !!}/{!! $pager->getCurrentPage() + 1 !!}">{!! __('następna') !!}</a>
                </span>
            <span class="page-item">
                    <a class="page-link" href="{!! $pager->getUrl() !!}/{!! $pager->getTotalPages() !!}">{!! __('ostatnia') !!}</a>
                </span>
        @endif
        <span class="page-item disabled"><a class="page-link" href="#">{!! __('Ilość stron') !!}: {!! $pager->getTotalPages() !!}, {!! __('Ilość wyników') !!}: {!! $pager->getTotalResults() !!}</a></span>
    </div>
@endif
