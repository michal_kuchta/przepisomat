<div id="alert-placeholder">
@if (!empty($info))
    <div class="alert alert-{{ $type }}">
        {{ $info }}
    </div>
@endif
</div>