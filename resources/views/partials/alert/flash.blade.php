@if(Session::has('flash_notification'))
    @foreach(Session::get('flash_notification') as $notification)
        @if ($notification->overlay)
            @include('partials.alert.modal', ['modalClass' => 'flash-modal', 'title' => $notification->title, 'body' => $notification->message])
        @else
            <div class="alert alert-{{ $notification->level }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! $notification->message !!}
            </div>
        @endif
    @endforeach
@endif
