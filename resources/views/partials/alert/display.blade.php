<div id="alert-placeholder">
@if (!empty($success))
    <div class="alert alert-success">
        {{ $success }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <div class="validation-summary-errors">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@include('partials.alert.flash')
</div>
