<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Przepisomat</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf" value="{{ csrf_token() }}">

{!! Fluent::scripts() !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>

<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
