<div class="checkbox disabled">
    <label>
        <input @if(!isset($required) || $required) data-rule-required="true" data-msg-required="Zaznaczenie tej zgody jest wymagane" @endif type="checkbox" name="is_rodo_checked" value="1" aria-required="true" @if(isset($model) && $model['is_rodo_checked']) checked="checked" @endif>
        <span></span>
        @if(!isset($required) || $required)
            <span style="color:red">*</span>
        @endif
        {!! __('Wyrażam zgodę na przetwarzanie moich danych osobowych przez Przepisomat, w celu zarejestrowania się w serwisie oraz zapisywania swoich przepisów.
        Oświadczam, iż dane zostały podane dobrowolnie i zapoznałam/zapoznałem się z poniższą klauzulą informacyjną oraz pouczeniem dotyczącym prawa dostępu
        do treści moich danych i możliwości ich poprawienia. Jestem świadomy, iż moja zgoda może być w każdym czasie odwołana.') !!}
    </label>
</div>
<span class="btn btn-info w-100 mb-2" data-toggle="modal" data-target="#rodo-modal">{!! __('Klauzula informacyjna') !!}</span>

<div class="modal" id="rodo-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Klauzula informacyjna') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Zamknij') }}">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{ __('Zgodnie z art. 13 ogólnego rozporządzenia o ochronie danych osobowych z dnia 27 kwietnia 2016 r. (dalej: RODO) informuję, iż Administratorem Państwa Danych Osobowych jest
Michał Kuchta zamieszkały Pagorzyna 146, 38-305 Lipinki;
z Administratorem można skontaktować się pod adresem e-mail: michal.kuchta@outlook.com;
Państwa dane osobowe będą przetwarzane na podstawie art. 6 ust. 1 lit. a i b) RODO w celu zarejestrowania się w serwisie oraz dodawania przepisów;
Państwa dane osobowe będą przechowywane do momentu usunięcia konta w serwisie www.przepisomat.kik-software.pl lub zgłoszenia wniosku o usunięcie Państwa danych;
w przypadku wyrażenia większej ilości zgód, podane dane osobowe będą przetwarzane również na podstawie art. 6 ust. 1 lit. a) RODO;
przysługuje Państwu prawo do żądania od administratora dostępu do danych osobowych, prawo ich sprostowania, usunięcia, ograniczenia przetwarzania, prawo do przenoszenia danych, prawo wniesienia sprzeciwu, prawo do cofnięcia zgody;
w przypadku stwierdzenia, że przetwarzanie Państwa danych osobowych narusza przepisy RODO, mają Państwo prawo wnieść skargę do Prezesa Urzędu Ochrony Danych Osobowych;
podanie danych osobowych jest dobrowolne, jednakże ich niepodanie skutkować będzie niemożliwością zarejestrowania się w serwisie oraz dodania przepisów;
podane dane osobowe nie będą przechowywane na serwerach w państwie trzecim;') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Zamknij') }}</button>
            </div>
        </div>
    </div>
</div>