@if (isset($categories) && count($categories) > 0)
    <div class="container">
        <nav class="navbar navbar-light bg-white shadow-sm mt-lg-4 mt-3 pt-0 pb-0" style="min-height: 50px;">
            <span class="font-weight-bold">{{ __('Kategorie') }}</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCategories" aria-controls="navbarCategories" aria-expanded="false" aria-label="{{ __('Rozwiń/Zwiń kategorie') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse category-level-0" id="navbarCategories">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav w-100 text-left">
                    @foreach ($categories as $category)
                        @include('partials.categories.item', ['category' => $category])
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
@endif