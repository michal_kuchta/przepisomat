@php /** @var \App\Category $category */ @endphp
<li class="nav-item dropdown w-100">
    <a class="nav-link d-inline-block" href="{!! $category->getUrl() !!}">{{ $category->getIntendedName() }}</a>
    @if ($category->has_children && $category->childrens->count() > 0)
        <button class="navbar-toggler p-0 border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarCategory_{{ $category->id }}" aria-controls="navbarCategory_{{ $category->id }}" aria-expanded="false" aria-label="{{ __('Rozwiń/Zwiń kategorie') }}">
            <span class="far fa-plus-square"></span>
            <span class="far fa-minus-square"></span>
        </button>
        @include('partials.categories.items', ['categories' => $category->childrens, 'parentId' => $category->id, 'level' => $category->level+1])
    @endif
</li>