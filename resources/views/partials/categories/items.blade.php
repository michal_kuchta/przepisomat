<div class="collapse navbar-collapse w-100 category-level-{{ $level }}" id="navbarCategory_{{ $parentId }}">
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav w-100 text-left">
        @foreach ($categories as $category)
            @include('partials.categories.item', ['category' => $category])
        @endforeach
    </ul>
</div>