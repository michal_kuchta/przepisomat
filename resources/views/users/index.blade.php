@extends('layouts.main')

@section('content')
    <h3 class="header">{{ __('Konto użytkownika') }}</h3>
    <div class="d-flex flex-wrap userAccount">
        <div class="p-2 d-flex flex-column m-2" style="max-width: 250px;">
            <a href="{{ route('users.getEdit') }}">
                <i class="fas fa-user-edit"></i>
                <div>{!! __('Dane osobowe') !!}</div>
            </a>
        </div>
        <div class="p-2 d-flex flex-column m-2" style="max-width: 250px;">
            <a href="{{ route('users.getPassword') }}">
                <i class="fas fa-unlock-alt"></i>
                <div>{!! __('Zmiana hasła') !!}</div>
            </a>
        </div>
        <div class="p-2 d-flex flex-column m-2" style="max-width: 250px;">
            <a href="{{ route('users.getSettings') }}">
                <i class="fas fa-users-cog"></i>
                <div>{!! __('Ustawienia konta') !!}</div>
            </a>
        </div>
        <div class="p-2 d-flex flex-column m-2" style="max-width: 250px;">
            <a href="{{ route('users.getDelete') }}">
                <i class="fas fa-trash-alt"></i>
                <div>{!! __('Usuń konto') !!}</div>
            </a>
        </div>
    </div>
@endsection
