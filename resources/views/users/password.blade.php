@extends('layouts.main')

@section('content')
    <h3 class="header">{{ __('Zmiana hasła') }}</h3>

    {!! FluentForm::standard()->rules($rules)->errors($errors) !!}
        {!! FluentForm::group()->password('old_password')->label(__('Stare hasło'))->required(true) !!}
        {!! FluentForm::group()->password('password')->label(__('Nowe hasło'))->required(true) !!}
        {!! FluentForm::group()->password('password_confirmation')->label(__('Powtórz nowe hasło'))->required(true) !!}

        {!! FluentForm::footer([
            FluentForm::submit('save', __('Zapisz zmiany'))->css('btn btn-success'),
            Fluent::link(route('users.getIndex'), __('Powrót'))->css('btn btn-info')
        ]) !!}
    {!! FluentForm::close() !!}
@endsection
