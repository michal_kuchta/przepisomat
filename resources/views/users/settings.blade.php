@extends('layouts.main')

@section('content')
    <h3 class="header">{{ __('Ustawienia konta') }}</h3>

    {!! FluentForm::standard() !!}

        <div class="row">
            <div class="col-lg-6">
                {!! FluentForm::group()->radios('share_type', [
                    \App\SharedRecipe::TYPE_NONE => __('Nie udostępniaj'),
                    \App\SharedRecipe::TYPE_CUSTOM => __('Udostępnij wybranym'),
                    \App\SharedRecipe::TYPE_ALL => __('Udostępnij wszystkim')
                ], $shareType ?? \App\SharedRecipe::TYPE_NONE)->label(__('Zasady udostępniania przepisów')) !!}
            </div>
            <div class="col-lg-6">
                <div id="shareEmailsBox" {!! !isset($shareType) || $shareType != \App\SharedRecipe::TYPE_CUSTOM ? 'style="display:none"' : '' !!}>
                    <multi-row-field-component
                        name="shared_emails"
                        :rows="{{ json_encode($sharedEmails ?? []) }}"
                        placeholder="{!! __('Adres e-mail') !!}"
                        label="{!! __('Udostępnij wybranym') !!}"
                    ></multi-row-field-component>
                </div>
            </div>
        </div>

        {!! FluentForm::footer([
            FluentForm::submit('save', __('Zapisz zmiany'))->css('btn btn-success'),
            Fluent::link(route('users.getIndex'), __('Powrót'))->css('btn btn-danger'),
        ]) !!}
    {!! FluentForm::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('input[type=radio][name=share_type]').change(function(){
                if ($(this).val() == {{ \App\SharedRecipe::TYPE_CUSTOM }})
                    $('#shareEmailsBox').show();
                else
                    $('#shareEmailsBox').hide();
            });
        });
    </script>
@endsection