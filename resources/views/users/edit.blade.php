@extends('layouts.main')

@section('content')
    <h3 class="header">{{ __('Edycja danych osobowych') }}</h3>

    {!! FluentForm::standard($model)->rules($rules)->errors($errors) !!}
        {!! FluentForm::group()->email('email')->label(__('Adres e-mail'))->required(true) !!}
        {!! FluentForm::group()->text('name')->label(__('Imię i Nazwisko')) !!}

        @include('partials.rodo', ['required' => false])

        {!! FluentForm::group()->password('password')->label(__('Podaj hasło'))->help(__('Hasło jest wymagane, aby móc wprowadzić żądane zmiany.')) !!}

        {!! FluentForm::footer([
            FluentForm::submit('save', __('Zapisz zmiany'))->css('btn btn-success'),
            Fluent::link(route('users.getIndex'), __('Powrót'))->css('btn btn-info')
        ]) !!}
    {!! FluentForm::close() !!}

    <div class="modal" id="rodo-unchecked-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Informacja') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Zamknij') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>{{ __('Drogi użytkowniku!') }}</p>
                    <p>{{ __('Zauważyliśmy, że odznaczyłeś zgodę na przetwarzanie danych osobowych. Zapisanie zmian z
                    odznaczoną zgodą RODO spowoduje usunięcie Twoich danych osobowych, Twojego konta oraz dodanych
                    przez Ciebie przepisów. Ta operacja jest nieodwracalna, więc zastanów się zanim zapiszesz zmiany.') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Zamknij') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('input[name="is_rodo_checked"]').val(this.checked);

            $('input[name="is_rodo_checked"]').change(function() {
                if(!this.checked) {
                    $('#rodo-unchecked-modal').modal({show: true})
                }
                $('input[name="is_rodo_checked"]').val(this.checked);
            });
        });
    </script>
@endsection