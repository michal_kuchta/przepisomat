@extends('layouts.main')

@section('content')
    <h3 class="header">{{ __('Usuwanie konta') }}</h3>

    <div class="panel panel-danger">
        <div class="panel-body">{{ __('Jesteś pewien, że chcesz usunąć swoje konto z naszego serwisu? Ta operacja jest nieodwracalna, więc zastanów się dwa razy zanim klikniesz przycisk Usuń. Podaj hasło, abyśmy mogli się upewnić, że to na pewno Ty jesteś właścicielem konta i chcesz je usunąć.') }}</div>
    </div>

    {!! FluentForm::standard() !!}
        {!! FluentForm::group()->password('password')->label(__('Hasło'))->required(true) !!}
        {!! FluentForm::footer([
            FluentForm::submit('delete', __('Tak, potwierdzam usuniecie'))->css('btn btn-danger'),
            Fluent::link(route('users.getIndex'), __('Nie, wracam'))->css('btn btn-success'),
        ]) !!}
    {!! FluentForm::close() !!}
@endsection
