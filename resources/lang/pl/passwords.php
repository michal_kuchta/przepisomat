<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi być o długości minimum 8 znaków.',
    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Link do zresetowania hasła został wysłany na Twój adres e-mail.',
    'token' => 'Klucz resetowania hasła jest niepoprawny.',
    'user' => "Niepoprawny login lub hasło.",

];
