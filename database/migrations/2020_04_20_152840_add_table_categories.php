<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('parent_id')->nullable();
            $table->string('ancestors');
            $table->integer('level');
            $table->integer('position');
            $table->string('symbol');
            $table->boolean('is_active');
            $table->boolean('has_children');
        });

        Schema::create('recipe_categories', function (Blueprint $table) {
            $table->integer('recipe_id');
            $table->integer('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('recipe_categories');
    }
}
