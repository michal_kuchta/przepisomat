<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultValuesToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('parent_id')->nullable()->change();
            $table->integer('level')->nullable()->change();
            $table->integer('position')->nullable()->change();
            $table->integer('is_active')->default(0)->change();
            $table->integer('has_children')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('parent_id')->nullable(false)->change();
            $table->integer('level')->nullable(false)->change();
            $table->integer('position')->nullable(false)->change();
            $table->integer('is_active')->default(null)->change();
            $table->integer('has_children')->default(null)->change();
        });
    }
}
