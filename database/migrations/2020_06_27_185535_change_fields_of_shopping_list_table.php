<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsOfShoppingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping_list', function (Blueprint $table) {
            $table->dropColumn('recipe_id');
            $table->float('total');
            $table->integer('unit_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping_list', function (Blueprint $table) {
            $table->integer('recipe_id');
            $table->dropColumn('total');
            $table->dropColumn('unit_id');
        });
    }
}
